# Project MySpace 2.0

## Main Theme of the project - Social Network for shring musical taste

__List Of Tasks To complete__

1. To figure out the main views of the site.
2. To create the initial building blocks (main reusable components) of the app.
    - Figure out which components are most frequently used, and build them first
    - Create the components with maximum abstraction to make them reusable
3. Create main functionalities of the site, starting with the largest and least complex (display feed) and going to the more complex (admin functionalities).

