import React from 'react';
import { ButtonVariants } from '../../../common/constants';
import { acceptFriendReq } from '../../../services/requests';
import useProfile from '../../../hooks/useProfile';
import { Button } from '@chakra-ui/react';
import { useQueryClient } from 'react-query';

interface AcceptButtonProps {
  variant?: ButtonVariants;
  toId: number;
  setFrChanged: React.Dispatch<React.SetStateAction<boolean>>
};

const AcceptButton: React.FC<AcceptButtonProps> = ({
  variant = ButtonVariants.PRIMARY,
  toId,
  setFrChanged
}) => {
  const { id, token } = useProfile();
  const queryClient = useQueryClient();
  
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    acceptFriendReq(id, toId, token)
      .then(console.log)
      .then(() => {
        setFrChanged(prev => !prev)
        queryClient.invalidateQueries('/users')
      });
  }
  
  return (
    <Button
      border="1px"
      borderColor="darkgrey"
      onClick={handleClick}
    >
      Accept
    </Button>
  );
};

export default AcceptButton;
