import React from 'react';
import { ButtonVariants } from '../../../common/constants';
import useProfile from '../../../hooks/useProfile';
import { sendFriendReq, unfriendReq } from '../../../services/requests';
import { Button } from '@chakra-ui/react';
import { useQueryClient } from 'react-query';

interface ConnectButtonProps {
  variant?: ButtonVariants | string;
  toId: number;
  setFrChanged: React.Dispatch<React.SetStateAction<boolean>>
};

const ConnectButton: React.FC<ConnectButtonProps> = ({
  variant = ButtonVariants.PRIMARY,
  toId,
  setFrChanged
}) => {
  const { id, token } = useProfile();
  const queryClient = useQueryClient();

  const handleSendFrReq = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    sendFriendReq(id, toId, token)
      .then(console.log)
      .then(() => {
        setFrChanged(prev => !prev);
        queryClient.invalidateQueries('/users');
      });
  };

  const handleUnfriendReq = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    unfriendReq(id, toId, token)
      .then(console.log)
      .then(() => {
        setFrChanged(prev => !prev);
        queryClient.invalidateQueries('/users');
      });
  }
  
  return (
    <>
      <Button
        variant={variant}
        borderColor="darkgrey"
        isDisabled={variant === ButtonVariants.OUTLINE ? true : false}
        onClick={
          variant === ButtonVariants.PRIMARY ? handleSendFrReq : handleUnfriendReq
        }
      >
        {variant === ButtonVariants.PRIMARY ? 'Connect' : 'Disconnect'}
      </Button>
    </>
  );
};

export default ConnectButton;
