import React from 'react';
import { ButtonVariants, FriendshipStatus } from '../../../common/constants';
import AcceptButton from './AcceptButton';
import ConnectButton from './ConnectButton';

interface FriendshipButtonProps {
  variant?: ButtonVariants;
  toId: number;
  areWeConnected: boolean;
  friendshipStatus: number;
  setFrChanged: React.Dispatch<React.SetStateAction<boolean>>
}

const FriendshipButton: React.FC<FriendshipButtonProps> = ({
  variant = ButtonVariants.PRIMARY,
  toId,
  areWeConnected,
  friendshipStatus,
  setFrChanged
}) => {

  if (!areWeConnected) {
    return (
      <ConnectButton toId={toId} setFrChanged={setFrChanged} />
    ); 
  }

  switch (friendshipStatus) {
    case FriendshipStatus.I_CAN_ACCEPT:
      return (
        <AcceptButton toId={toId} setFrChanged={setFrChanged} />
      );
    case FriendshipStatus.I_CANNOT_ACCEPT:
      return (
        <ConnectButton toId={toId} setFrChanged={setFrChanged} variant={ButtonVariants.OUTLINE} />
      );
    case FriendshipStatus.FRIENDS:
      return (
        <ConnectButton toId={toId} variant={ButtonVariants.SECONDARY} setFrChanged={setFrChanged} />
      );
    default: return (
      <div>something went wrong</div>
    )
  };
};

export default FriendshipButton;
