import { Button } from '@chakra-ui/button';
import { useColorModeValue } from '@chakra-ui/react';
import { NavLink, useLocation } from 'react-router-dom';
import { ButtonVariants } from '../../common/constants';

const UsersButton: React.FC<{variant?: ButtonVariants}> = ({ variant = ButtonVariants.PRIMARY }) => {
  const location = useLocation();
  let btnColorInactive = useColorModeValue('gray.600', 'gray.700')
  let btnColorActive = useColorModeValue('blue.500', 'red.500')
  let btnColor = btnColorInactive;
  if (location.pathname === '/users') btnColor = btnColorActive;

  return (
    <NavLink to="/users">
      <Button 
        px={[4, null, 6]}
        py={[2, 4, 6]}
        fontSize={['md', 'lg', '2xl']}
        bg={btnColor}
        color="white"
      >
        Users
      </Button>
    </NavLink>
  );
};

export default UsersButton;