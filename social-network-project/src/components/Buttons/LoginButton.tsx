import { Button } from '@chakra-ui/button';
import { NavLink } from 'react-router-dom';
import { ButtonVariants } from '../../common/constants';
import { useLocation } from 'react-router-dom';

const LoginButton: React.FC<{variant?: ButtonVariants}> = ({ variant = ButtonVariants.PRIMARY }) => {
  const isOnLoginPage = useLocation().pathname === '/login'

  return (
    <>
      {isOnLoginPage ? (
        <Button
          variant='outline'
          cursor="default"
          _hover={{ background: 'unset' }}
          _active={{ background: 'unset' }}
        >
          Log In
        </Button>
      ) : (
        <NavLink to="/login">
          <Button variant={variant}>
            Log In
          </Button>
        </NavLink>
      )}
    </>
  )
};

export default LoginButton;
