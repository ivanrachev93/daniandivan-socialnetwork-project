import { Button } from '@chakra-ui/button';
import { NavLink } from 'react-router-dom';
import { ButtonVariants } from '../../common/constants'

const SignUpButton: React.FC<{variant?: ButtonVariants}> = ({ variant = ButtonVariants.PRIMARY }) => {
  return (
    <NavLink to="/register">
      <Button variant={variant}>Join for Free</Button>
    </NavLink>
  )
};

export default SignUpButton;