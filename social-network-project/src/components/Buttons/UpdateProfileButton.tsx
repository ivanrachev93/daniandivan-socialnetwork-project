import { Button } from '@chakra-ui/button';
import React from 'react';

const UpdateProfileButton: React.FC<{ toggle: React.Dispatch<React.SetStateAction<boolean>> }> = (
  { toggle } 
) => {
  return (
    <Button onClick={() => toggle(prev => !prev)}>
      Update Profile
    </Button>
  );
};

export default UpdateProfileButton;
