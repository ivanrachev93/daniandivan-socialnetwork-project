import { Button } from '@chakra-ui/button';
import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { ButtonVariants } from '../../common/constants';
import { AuthContext } from '../../providers/AuthContext';
import { sendLogoutRequest } from '../../services/requests';

const LogoutButton: React.FC<{variant?: ButtonVariants}> = ({ variant = ButtonVariants.PRIMARY }) => {
  const { user: profile, setAuth } = useContext(AuthContext);
  const navigate = useNavigate();

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    localStorage.removeItem('token');
    sendLogoutRequest(profile.token)
      .then(console.log)
      .then(() => setAuth({
        username: '',
        id: 0,
        avatar: null,
        token: '',
        role: 0,
        email: '',
        isLoggedIn: false,
        friends: [],
      }))
      .then(() => navigate('/login'));
  };
  
  return (
    <Button variant={variant} onClick={handleClick}>
      Log Out
    </Button>
  );
};

export default LogoutButton;
