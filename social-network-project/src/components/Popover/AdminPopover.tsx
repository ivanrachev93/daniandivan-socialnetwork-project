import { useDisclosure } from '@chakra-ui/hooks';
import { IconButton } from '@chakra-ui/react';
import { FaRegEdit } from 'react-icons/fa';
import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverArrow,
  PopoverCloseButton
} from '@chakra-ui/popover';
import AdminUserForm from '../Admin/AdminUserOptions/AdminUserForm';

const AdminPopover: React.FC<{ userId: number }> = ({ userId }) => {
  const { onOpen, onClose, isOpen } = useDisclosure()

  return (
    <Popover
      isOpen={isOpen}
      onOpen={onOpen}
      onClose={onClose}
      placement="bottom"
      closeOnBlur={false}
    >
      <PopoverTrigger>
        <IconButton aria-label="edit-button" size='sm' icon={<FaRegEdit />} />
      </PopoverTrigger>
      <PopoverContent p={5}>
        <PopoverArrow />
        <PopoverCloseButton />
        <AdminUserForm userId={userId} />
      </PopoverContent>
    </Popover>
  );
};

export default AdminPopover;
