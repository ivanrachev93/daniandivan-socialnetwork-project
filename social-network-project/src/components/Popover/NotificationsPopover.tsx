import NotificationsBell from '../Notifications/NotificationsBell';
import NotificationsContent from '../Notifications/NotificationsContent';
import { useDisclosure } from '@chakra-ui/hooks';
import useFriendRequests from '../../hooks/friends/useFriendRequests';
import {
  Button,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverArrow,
  PopoverCloseButton
} from '@chakra-ui/react';

const NotificationsPopover: React.FC = () => {
  const { onOpen, onClose, isOpen } = useDisclosure();
  const friendReqs = useFriendRequests();

  return (
    <Popover
      isOpen={isOpen}
      onOpen={onOpen}
      onClose={onClose}
      placement="bottom"
      closeOnBlur={false}
    >
      <PopoverTrigger>
        <Button
          bgColor="transparent"
          _hover={{ background: 'transparent' }}
          _active={{ background: 'transparent' }}
          p={[1, 2]}
        >
          <NotificationsBell />
        </Button>
      </PopoverTrigger>
      <PopoverContent p={5}>
        <PopoverArrow />
        <PopoverCloseButton />
        <NotificationsContent
          friendRequests={friendReqs}
        />
      </PopoverContent>
    </Popover>
  );
};

export default NotificationsPopover;
