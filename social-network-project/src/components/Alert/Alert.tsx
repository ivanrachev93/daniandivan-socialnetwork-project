import {
  Alert,
  AlertIcon,
  Text,
} from '@chakra-ui/react';

interface AlertProps {
  text: string;
}

const AlertPopUp: React.FC<AlertProps> = ({ text }) => {
  return (
    <Alert status="error">
      <AlertIcon />
      <Text fontSize={['xs', 'md']}>{text}</Text>
    </Alert>
  );
};

export default AlertPopUp;