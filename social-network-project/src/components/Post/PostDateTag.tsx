import { Tag } from '@chakra-ui/tag';
import React from 'react';

const PostDateTag: React.FC<{ createdOn: string }> = ({ createdOn }) => {
  const date = createdOn.split(' ') // Todo

  return (
    <Tag fontSize="sm">{`${date[1]} ${date[2]}`}</Tag>
  )
}

export default PostDateTag;
