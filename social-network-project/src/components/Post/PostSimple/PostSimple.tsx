import React from 'react';
import { PostSimpleType } from '../../../types/PostTypes'
import PostBase from '../PostBase';

interface PostSimpleProps {
  post: PostSimpleType;
}

const PostSimple: React.FC<PostSimpleProps> = ({ post }) => {
  return (
    <PostBase 
      embed={post.embed}
      picture={post.picture}
      content={post.content}
      createdOn={post.createdOn}
    />
  );
};

export default PostSimple;
