import { useColorModeValue } from '@chakra-ui/color-mode';
import { Box, Text, VStack, HStack, Divider } from '@chakra-ui/layout';
import React from 'react';
import { SimpleUserType } from '../../types/UserTypes';
import PostDateTag from './PostDateTag';
import UsernameWithPic from '../CardComponents/UsernameWithPic/UsernameWithPic';
import { extractVideoId } from '../../common/helpers';
import Picture from '../Picture/Picture';
import Embed from '../Embed/YoutubeEmbed'

interface PostBaseProps { 
  children?: React.ReactNode;
  picture: string | undefined;
  embed: string | undefined;
  content: string;
  createdOn: string;
  author?: SimpleUserType;
  comments?: React.ReactNode;
  postOptions?: React.ReactNode;
}

const PostBase: React.FC<PostBaseProps> = (
  { children, picture, embed, content, createdOn, author, comments, postOptions }
) => {
  const boxColor = useColorModeValue('gray.50', 'gray.700');

  return (
    <Box borderRadius={2} overflow="hidden" fontSize={18}>
      <Box bg={boxColor}>
        {picture && <Picture src={picture} />}
        {embed && <Embed src={extractVideoId(embed)}/>}
        <VStack spacing={4} p={6} align="start">
          <HStack w="full" justify="space-between">
            <HStack flexGrow={1} alignItems="center">
              {author && 
                <UsernameWithPic
                  id={author.id}
                  username={author.username}
                  avatar={author.avatar}
                  size={2.5}
                />
              }
              <PostDateTag createdOn={createdOn} />
            </HStack>
            {postOptions}
          </HStack>
          <Divider />
          <Text fontWeight="light">{content}</Text>
          {children}
        </VStack>
      </Box>
      <Divider />
      {comments && <Box>{comments}</Box>}
    </Box>
  );
};

export default PostBase;
