import { Divider, Flex, HStack, Text } from '@chakra-ui/layout';
import React from 'react';
import useProfile from '../../../hooks/useProfile';
import { PostDetailedType } from '../../../types/PostTypes'
import PostBase from '../PostBase';
import PostReactions from './PostReactions';
import CreateComment from '../../InputModals/Comment/CreateComment'
import { FaComment } from 'react-icons/fa';
import PostOptions from './PostOptions';
import PostComments from './PostComments';

const PostDetailed: React.FC<{ post: PostDetailedType }> = ({ post }) => {
  const { id: myId } = useProfile();
  const allReactions = post.likes;
  const myReaction = post.likes.length > 0
    ? post.likes.find(user => user.id === myId)?.reaction || null
    : null;
    
  return (
    <PostBase
      embed={post.embed}
      picture={post.picture}
      content={post.content}
      createdOn={post.createdOn}
      author={post.author}
      comments={<PostComments comments={post.comments} />}
      postOptions={<PostOptions post={post} />}
    >
      <Divider />
      <Flex direction="row" w="full" justify="space-between">
        <PostReactions
          postId={post.id}
          myReaction={myReaction}
          postReactions={allReactions}
        />
        <HStack>
          <FaComment />
          <Text>{post.comments.length} Comment{post.comments.length === 1 ? '' : 's'}</Text>
        </HStack>
      </Flex>
      <CreateComment postId={post.id} />
    </PostBase>
  );
};

export default PostDetailed;
