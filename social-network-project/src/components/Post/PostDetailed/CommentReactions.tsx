import { Button, Flex, Text, useBreakpointValue, useColorModeValue } from '@chakra-ui/react';
import React, { useState } from 'react';
import { AiFillHeart, AiFillLike, AiOutlineHeart, AiOutlineLike } from 'react-icons/ai';
import { FcLike, FcLikePlaceholder } from 'react-icons/fc';
import { Reactions } from '../../../common/constants';
import { updateReactionsCount } from '../../../common/helpers';
import useProfile from '../../../hooks/useProfile';
import { sendCommentReaction } from '../../../services/requests';
import { ReactionsType } from '../../../types/PostTypes';

interface CommentReactionsProps {
  commentId: number;
  commentReactions: ReactionsType[];
  myReaction: Reactions | null;
}

const CommentReactions: React.FC<CommentReactionsProps> = ({
  commentId,
  commentReactions,
  myReaction
}) => {
  const [reactionsCounter, setReactionsCounter] = useState({
    likes: commentReactions.filter(user => user.reaction === Reactions.LIKE).length,
    loves: commentReactions.filter(user => user.reaction === Reactions.LOVE).length,
  });
  const [reaction, setReaction] = useState({
    reactionState: myReaction,
  });
  const { token } = useProfile();

  const handleReaction = (event: React.MouseEvent<HTMLButtonElement>, lastReaction: Reactions) => {
    event.preventDefault();
    lastReaction === reaction.reactionState
      ? setReaction({reactionState: null})
      : setReaction({reactionState: lastReaction});
    updateReactionsCount(
      reaction.reactionState,
      lastReaction,
      setReactionsCounter
    );

    sendCommentReaction(
      commentId,
      {reaction: lastReaction},
      token
    )
      .then(console.log);
  };

  const buttonSizes = useBreakpointValue(['1.5em', '2em'])
  const color = useColorModeValue('gray.500', 'gray.400');
  const colorHover = useColorModeValue('blue.400', 'red.400');

  const isLiked = reaction.reactionState === Reactions.LIKE;
  const isLoved = reaction.reactionState === Reactions.LOVE;

  return (
    <Flex>
      <Flex alignItems="center">
        <Button
          px={2}
          backgroundColor="transparent"
          color={isLiked ? colorHover : color}
          _hover={{ background: 'transparent', filter: 'brightness(130%)', color: colorHover }}
          _active={{ background: 'transparent' }}
          transition="filter .2s ease, color .2s ease"
          onClick={(event) => handleReaction(event, Reactions.LIKE)}
        >
          {isLiked
            ? <AiFillLike size={buttonSizes} />
            : <AiOutlineLike size={buttonSizes} />}
        </Button>
        <Text fontWeight="bold" px={1}>
          {reactionsCounter.likes}
        </Text>
      </Flex>
      <Flex alignItems="center">
        <Button
          px={2}
          backgroundColor="transparent"
          color={isLoved ? colorHover : color}
          _hover={{ background: 'transparent', filter: 'brightness(130%)', color: colorHover }}
          _active={{ background: 'transparent' }}
          transition="filter .2s ease, color .2s ease"
          onClick={(event) => handleReaction(event, Reactions.LOVE)}
        >
          {isLoved
            ? <AiFillHeart size={buttonSizes} />
            : <AiOutlineHeart size={buttonSizes}/>
          }
        </Button>
        <Text fontWeight="bold" px={1}>
          {reactionsCounter.loves}
        </Text>
      </Flex>
    </Flex>
  );
};

export default CommentReactions;
