import { Button } from '@chakra-ui/button';
import { Box, Divider, HStack, Text, VStack } from '@chakra-ui/layout';
import { useColorModeValue } from '@chakra-ui/react';
import React from 'react';
import { FaTrash } from 'react-icons/fa';
import useComments from '../../../hooks/comments/useComments';
import useProfile from '../../../hooks/useProfile';
import { CommentType } from '../../../types/PostTypes';
import UsernameWithPic from '../../CardComponents/UsernameWithPic/UsernameWithPic';
import EditComment from '../../InputModals/Comment/EditComment';
import CommentReactions from './CommentReactions';

const Comment: React.FC<{ comment: CommentType }> = ({ comment }) => {
  const { isAdmin, id: myId } = useProfile();
  const allReactions = comment.likes;
  const myReaction = allReactions.length > 0
    ? allReactions.find(user => user.id === myId)?.reaction || null
    : null;
  const { deleteComment } = useComments();
  const commentBelongsToMe = comment.author.id === myId;


  return (
    <Box>
      <HStack spacing={1} justify="space-between">
        <UsernameWithPic
          username={commentBelongsToMe ? `${comment.author.username} (You)` : comment.author.username}
          avatar={comment.author.avatar || 'default.png'}
          id={comment.author.id}
          size={2}
        />
        {(isAdmin || commentBelongsToMe) && (
          <HStack spacing={1}>
            <Button p={[2, 4]} onClick={() => deleteComment(comment.id)}>
              <FaTrash />
            </Button>
            <EditComment
              commentId={comment.id}
              commentValue={{ content: comment.content, embed: '' }}
            />
          </HStack>
        )}
      </HStack>
      <Text mt={[3, 6]}>{comment.content}</Text>
      <Divider h={[2, 4]} mb={[2, 4]}/>
      <CommentReactions
          commentId={comment.id}
          myReaction={myReaction}
          commentReactions={allReactions}
      />
      <Divider h={[2, 4]} />
    </Box>
  )
}

const PostComments: React.FC<{ comments: CommentType[] }> = ({ comments }) => {
  const commentsBg = useColorModeValue('#e8e8f1', 'gray.800')

  return (
    <>
    {comments.length > 0 &&
      <VStack
        p={6}
        align="stretch"
        spacing={8}
        bg={commentsBg} 
      >
        {comments.map(c => <Comment key={c.id} comment={c}/>)}
      </VStack>
    }
    </>
  )
}

export default PostComments;