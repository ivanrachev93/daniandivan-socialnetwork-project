import { Button, useColorModeValue } from '@chakra-ui/react';
import React from 'react';

interface ReactionButtonProps {
  children: React.ReactNode;
  handleClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const ReactionButton: React.FC<ReactionButtonProps> = (children, handleClick) => {
  const color = useColorModeValue('gray.700', 'gray.500');
  const colorHover = useColorModeValue('blue.500', 'red.500');

  return (
    <Button
      px={2}
      backgroundColor="transparent"
      color={color}
      _hover={{ background: 'transparent', filter: 'brightness(130%)', color: colorHover }}
      _active={{ background: 'transparent' }}
      transition="filter .2s ease, color .2s ease"
      onClick={handleClick}
    >
      {children}
    </Button>
  )
} 

export default ReactionButton;
