import { Button } from '@chakra-ui/button';
import { HStack } from '@chakra-ui/layout';
import React from 'react';
import { FaTrash } from 'react-icons/fa';
import usePosts from '../../../hooks/posts/usePosts';
import useProfile from '../../../hooks/useProfile';
import { PostDetailedType } from '../../../types/PostTypes';
import EditPost from '../../InputModals/Post/EditPost';

const PostOptions: React.FC<{ post: PostDetailedType }> = ({ post }) => {
  const { isAdmin, id: myId } = useProfile();
  const { deletePost } = usePosts();

  return (
    <>
      {(isAdmin || post.author.id === myId) && (
      <HStack spacing={1}>
        <Button
          onClick={() => deletePost(post.id)}
          p={[2, 4]}
        >
          <FaTrash />
        </Button>
        <EditPost 
          postValue={{
            content: post.content,
            isPublic: post.isPublic,
            embed: '',
            picture: '',
          }}
          postId={post.id}
        />
      </HStack>
      )}
    </>
  )
}

export default PostOptions;
