import { AspectRatio } from '@chakra-ui/layout';

const Embed: React.FC<{ src: string }> = ({ src }) => {
  return (
    <AspectRatio w="full" ratio={16/9}>
      <iframe
        src={`https://www.youtube-nocookie.com/embed/${src}`}
        title="YouTube video player"
        allow="autoplay"
        allowFullScreen
      />
    </AspectRatio>
  )
}

export default Embed;
