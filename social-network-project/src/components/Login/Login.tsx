import { useContext, useState } from 'react';
import { sendLoginRequestJSON } from '../../services/requests';
import { UserObject } from '../../types/UserFormTypes';
import { decodeToken } from '../../common/helpers';
import { AuthContext } from '../../providers/AuthContext';
import AlertPopUp from '../Alert/Alert';
import PasswordInput from '../FormComponents/PasswordInput/PasswordInput';
import UsernameInput from '../FormComponents/UsernameInput/UsernameInput';

import { Button, VStack } from '@chakra-ui/react'

const initialUser: UserObject = {
  username: {
    touched: false,
    value: ''
  },
  password: {
    touched: false,
    value: ''
  }
};

const emptyAlertObject = {
  isShowing: false,
  text: '',
};

const Login: React.FC = () => {
  const [user, setUser] = useState(initialUser);
  const [alert, toggleAlert] = useState(emptyAlertObject);
  const { setAuth } = useContext(AuthContext);
  
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!user.username.value || !user.password.value) {
      toggleAlert(prev => ({
        text: 'No credentials added! Please, add username and password!',
        isShowing: true
      }));

      return;
    }

    sendLoginRequestJSON({
      username: user.username.value,
      password: user.password.value,
    })
      .then(data => {
        if (data.message) {
          toggleAlert({
            text: 'Wrong credentials! No such username or password available in the database!',
            isShowing: true
          });

          return;
        }

        const token: string = data.token;
        const userInfo = decodeToken(token);
        localStorage.setItem('token', token);
        setAuth((prev) => ({
          ...prev,
          token,
          isLoggedIn: true,
          ...userInfo,
        }));
      });
  };
  
  return (
      <form onSubmit={handleSubmit}>
        <VStack spacing={5} position="relative">
          {alert.isShowing && <AlertPopUp text={alert.text} />}
          <UsernameInput 
            username={user.username.value}
            isTouched={user.username.touched!}
            setUsername={setUser}
            />
          <PasswordInput 
            password={user.password.value}
            isTouched={user.password.touched!}
            setPassword={setUser}
            />
          <Button type="submit" w="full" py={[4, 6]} fontSize={['xl', '2xl']}>Log In</Button>
        </VStack>
      </form>
  );
};

export default Login;
