import { useRef, useState } from 'react';
import { sendRegisterRequest } from '../../services/requests';
import { UserObject } from '../../types/UserFormTypes';
import AlertPopUp from '../Alert/Alert';
import PasswordInput from '../FormComponents/PasswordInput/PasswordInput';
import UsernameInput from '../FormComponents/UsernameInput/UsernameInput';
import EmailInput from '../FormComponents/EmailInput/EmailInput';
import PictureInput from '../FormComponents/PictureInput/PictureInput';

import { Button, useToast, VStack } from '@chakra-ui/react';

const initialUser: UserObject = {
  username: {
    touched: false,
    value: '',
  },
  password: {
    touched: false,
    value: '',
  },
  email: {
    touched: false,
    value: '',
  }
};

const emptyAlertObject = {
  isShowing: false,
  text: '',
};

const Register: React.FC = () => {
  const [userCredentials, setUserCredentials] = useState(initialUser);
  const [alert, toggleAlert] = useState(emptyAlertObject);
  const toast = useToast();
  const { current: formData } = useRef(new FormData());

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!userCredentials.username.value || !userCredentials.password.value) {
      toggleAlert({
        text: 'No Credentials added! Please, add username and password!',
        isShowing: true
      });

      return;
    }

    sendRegisterRequest(formData)
      .then((data) => {
        if (!data.message) {
          toast({
            title: 'Registration was successful',
            description: `You registered successfully as ${data.username}`,
            status: 'success',
            position: 'top',
            duration: 5000,
            isClosable: true,
          });
          setUserCredentials(initialUser);
        } else {
          toast({
            title: 'Error',
            description: 'User already exists',
            status: 'error',
            position: 'top',
            duration: 5000,
            isClosable: true,
          });
        }
      });
  };

  return (
    <>
      <form className="register-form" onSubmit={handleSubmit}>
        <VStack spacing={5}>
          {alert.isShowing && <AlertPopUp text={alert.text} />}
          <UsernameInput
            username={userCredentials.username.value}
            isTouched={userCredentials.username.touched!}
            setUsername={setUserCredentials}
            formData={formData}
            />
          <PasswordInput
            password={userCredentials.password.value}
            isTouched={userCredentials.password.touched!}
            setPassword={setUserCredentials}
            formData={formData}
            />
          <EmailInput 
            email={userCredentials.email?.value}
            isTouched={userCredentials.email?.touched!}
            setEmail={setUserCredentials}
            formData={formData}
            />
          <PictureInput formData={formData} />
          <Button type="submit" w="full" py={[4, 6]} fontSize={['xl', '2xl']}>Sign Up</Button>
        </VStack>
      </form>
    </>
  );
};

export default Register;
