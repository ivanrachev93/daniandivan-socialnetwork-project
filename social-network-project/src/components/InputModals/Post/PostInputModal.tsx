import { Button, ButtonGroup } from '@chakra-ui/button';
import { FormLabel } from '@chakra-ui/form-control';
import { Flex, HStack, VStack } from '@chakra-ui/layout';
import { Switch } from '@chakra-ui/switch';
import { Textarea } from '@chakra-ui/textarea';
import React, { useState, useRef, useEffect } from 'react';
import usePosts from '../../../hooks/posts/usePosts'
import { CreatePostType } from '../../../types/PostTypes';
import InputModal from '../InputModal';
import { Input } from '@chakra-ui/react';
import { FaImage, FaLink } from 'react-icons/fa';
import PictureInput from '../../FormComponents/PictureInput/PictureInput';

const emptyPostValue = {
  content: '',
  embed: '',
  longitude: '',
  latitude: '',
  picture: '',
  isPublic: false,
}

interface PostInputModalProps {
  initialPostValue?: CreatePostType;
  title: string;
  isOpen: boolean;
  onClose: () => void;
  type: 'add' | 'edit'
  postId?: number;
}

const formData = new FormData();
formData.set('isPublic', `${emptyPostValue.isPublic}`);

const PostInputModal: React.FC<PostInputModalProps> = (
  { initialPostValue = emptyPostValue, isOpen, onClose, title, type, postId }
) => {

  const initialState = { ...emptyPostValue, ...initialPostValue }
  const { current: fd } = useRef(formData);
  const [postValue, setPostValue] = useState(initialState)
  const { addPost, editPost } = usePosts();
  
  enum OptionTypes { 'picture', 'embed' };
  const [activeOption, setActiveOption] = useState(
    initialState.embed === '' ? OptionTypes.picture : OptionTypes.embed
  );

  const closeModal = () => {
    setPostValue(initialState)
    onClose()
  };

  const toggleIsPublic = (value: boolean) => {
    setPostValue(prev => ({ ...prev, isPublic: value }))
  }

  const setContent = (value: string) => {
    setPostValue(prev => ({ ...prev, content: value }))
  }

  const setEmbed = (value: string) => {
    setPostValue(prev => ({ ...prev, embed: value }))
  }

  const toggleActiveOption = (option: OptionTypes) => {
    setActiveOption(option);
    setPostValue(prev => ({ ...prev, embed: '' }))
    fd.set('file', '')
    fd.set('embed', '')
  }

  const handleSubmit = (event: React.SyntheticEvent) => {
    event.preventDefault();
    closeModal();
    
    fd.set('isPublic', `${postValue.isPublic}`)
    fd.set('content', postValue.content)
    fd.set('embed', postValue.embed)

    if (type === 'add') addPost(fd);
    if (type === 'edit') editPost(postId!, fd);
  }

  const validatePost = () => {
    if ((!postValue.content && !postValue.embed) || postValue.content!.length < 4) return false;
    return true;
  }

  const ModalFooter: React.FC = () => {
    return (
      <>
      <Flex alignItems='center' justify="space-between" w="full">
        <HStack>
          <FormLabel htmlFor='post-is-public' mb='0' fontSize="lg">
            Public
          </FormLabel>
          <Switch
            isChecked={postValue.isPublic}
            onChange={e => toggleIsPublic(e.target.checked)}
            id='post-is-public'
            size="lg"
          />
        </HStack>
        <Button type="submit" disabled={!validatePost()}>Submit</Button>
      </Flex>
      </>
    );
  }

  return (
    <InputModal
      title={title}
      isOpen={isOpen}
      onClose={closeModal}
      footer={<ModalFooter />}
      handleSubmit={handleSubmit}
    >
      <VStack>
        <HStack w="full">
          {activeOption === OptionTypes.embed && (
            <Input 
              value={postValue.embed}
              onChange={(e) => setEmbed(e.target.value)}
              my={2}
              placeholder="Paste your link..."
            />
          )}
          {activeOption === OptionTypes.picture && (
            <PictureInput hideLabel={true} formData={fd} />
          )}
          <ButtonGroup>
            <Button
              px={4}
              h="full"
              onClick={() => toggleActiveOption(OptionTypes.picture)}
              disabled={activeOption === OptionTypes.picture}
            >
              <FaImage/>
            </Button>
            <Button
              px={4}
              h="full"
              onClick={() => toggleActiveOption(OptionTypes.embed)}
              disabled={activeOption === OptionTypes.embed}
            >
              <FaLink />
            </Button>
          </ButtonGroup>
        </HStack>
        <Textarea rows={4} onChange={e => setContent(e.target.value)} value={postValue.content} />
      </VStack>
    </InputModal>
  );
};

export default PostInputModal;
