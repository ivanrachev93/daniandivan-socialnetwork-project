import { useDisclosure } from '@chakra-ui/hooks';
import React from 'react';
import AddButton from '../AddButton';
import PostInputModal from './PostInputModal';

const CreatePost: React.FC = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <AddButton title="Create a new post..." onOpen={onOpen} />
      <PostInputModal title="Create a Post" isOpen={isOpen} onClose={onClose} type="add" />
    </>
  )
}

export default CreatePost;
