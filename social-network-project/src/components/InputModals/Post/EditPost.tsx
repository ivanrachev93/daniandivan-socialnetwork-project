import { useDisclosure } from '@chakra-ui/hooks';
import React from 'react';
import { CreatePostType } from '../../../types/PostTypes';
import EditButton from '../EditButton';
import PostInputModal from './PostInputModal';

interface EditPostProps {
  postValue: CreatePostType;
  postId: number;
}

const EditPost: React.FC<EditPostProps> = ({ postValue, postId }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <EditButton title="Edit" onOpen={onOpen} />
      <PostInputModal
        title="Edit Post"
        isOpen={isOpen}
        onClose={onClose}
        type="edit"
        initialPostValue={postValue}
        postId={postId}
      />
    </>
  )
}

export default EditPost;
