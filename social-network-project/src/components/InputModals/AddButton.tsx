import { Button } from '@chakra-ui/button';
import { useColorModeValue } from '@chakra-ui/color-mode';
import { Image } from '@chakra-ui/image';
import { Box, HStack } from '@chakra-ui/layout';
import React from 'react';
import { Link } from 'react-router-dom';
import useProfile from '../../hooks/useProfile';

interface AddButtonProps {
  title: string;
  onOpen: () => void;
  darker?: boolean;
}

const AddButton: React.FC<AddButtonProps> = ({ title, onOpen, darker }) => {
  const { avatar, id } = useProfile();
  const bgColor = useColorModeValue(
    darker ? 'rgba(0,0,0,0.1)' : 'gray.50',
    'gray.800'
  )
  
  return (
    <HStack spacing={[2, 4]}>
      <Box width={['15%', '10%']}>
        <Link to={`/users/${id}`}>
          <Image 
            src={avatar}
            boxSize='100%'
            borderRadius="100%"
            objectFit='cover'
          />
        </Link>
      </Box>
      <Button 
        onClick={onOpen}
        w="full"
        bg={bgColor}
        _hover={{filter: 'brightness(120%)'}}
        _active={{filter: 'brightness(120%)'}}
        transition="filter .2s ease"
        display="flex"
        alignItems="center"
        justifyContent="start"
        px={[4, 5]}
        py={[5, 6]}
        fontWeight="semibold"
        fontSize={[16, 20]}
        borderRadius={4}
        color="gray.400"
      >
        {title}
      </Button>
    </HStack>
  );
};

export default AddButton;
