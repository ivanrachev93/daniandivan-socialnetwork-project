import { useColorModeValue } from '@chakra-ui/color-mode';
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from '@chakra-ui/modal';

interface InputModalProps {
  title: string;
  isOpen: boolean;
  onClose: () => void;
  children: React.ReactNode;
  footer?: React.ReactNode;
  handleSubmit?: (e: React.SyntheticEvent) => void;
}

const InputModal: React.FC<InputModalProps> = ({
  title,
  isOpen,
  onClose,
  children,
  footer,
  handleSubmit,
}) => {
  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose} isCentered size="xl">
        <ModalOverlay />
        <ModalContent m={3} bg={useColorModeValue('gray.50', 'gray.800')}>
          <ModalHeader fontSize="2xl">{title}</ModalHeader>
          <ModalCloseButton />
          {!!handleSubmit 
            ? <form onSubmit={handleSubmit}>
                <ModalBody>{children}</ModalBody>
                <ModalFooter>{footer}</ModalFooter>
              </form>
            : <>
                <ModalBody>{children}</ModalBody>
                <ModalFooter />
              </>}
        </ModalContent>
      </Modal>
    </>
  );
};

export default InputModal;
