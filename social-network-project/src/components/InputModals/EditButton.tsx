import { Button } from '@chakra-ui/button';
import React from 'react';
import { FaEdit } from 'react-icons/fa';

interface EditButtonProps {
  title?: string;
  onOpen: () => void;
}

const EditButton: React.FC<EditButtonProps> = ({ title, onOpen }) => {
  return (
    <Button onClick={onOpen} p={[2, 4]}>
      <FaEdit />
    </Button>
  )
}

export default EditButton;
