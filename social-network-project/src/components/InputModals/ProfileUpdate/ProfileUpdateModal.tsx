import UpdateProfileForm from '../../FormComponents/UpdateProfileForm/UpdateProfileForm';
import InputModal from '../InputModal';

interface ProfileUpdateModalProps {
  title: string;
  isOpen: boolean;
  onClose: () => void;
};

const ProfileUpdateModal: React.FC<ProfileUpdateModalProps> = ({
  title,
  isOpen,
  onClose,
}) => {
  return (
    <InputModal
      title={title}
      isOpen={isOpen}
      onClose={onClose}
    >
      <UpdateProfileForm />
    </InputModal>
  );
};

export default ProfileUpdateModal;
