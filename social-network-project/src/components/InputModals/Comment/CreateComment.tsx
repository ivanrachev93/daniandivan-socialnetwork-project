import { useDisclosure } from '@chakra-ui/hooks';
import React from 'react';
import AddButton from '../AddButton';
import CommentInputModal from './CommentInputModal';

const CreateComment: React.FC<{ postId: number }> = ({ postId }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <AddButton darker={true} title="Leave a comment..." onOpen={onOpen} />
      <CommentInputModal
        title="Create a Comment"
        type="add"
        isOpen={isOpen}
        onClose={onClose}
        postId={postId}
      />
    </>
  )
}

export default CreateComment;
