import { useDisclosure } from '@chakra-ui/hooks';
import React from 'react';
import { CreateCommentType } from '../../../types/PostTypes';
import EditButton from '../EditButton';
import CommentInputModal from './CommentInputModal';

interface EditCommentProps {
  commentValue: CreateCommentType;
  commentId: number;
}

const EditComment: React.FC<EditCommentProps> = ({ commentId, commentValue }) => {
  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <>
      <EditButton title="Edit" onOpen={onOpen}></EditButton>
      <CommentInputModal
        title="Edit Comment"
        type="edit"
        isOpen={isOpen}
        onClose={onClose}
        commentId={commentId}
        initialCommentValue={commentValue}
      />
    </>
  )
}

export default EditComment;
