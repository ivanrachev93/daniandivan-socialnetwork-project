import { Button } from '@chakra-ui/button';
import { Textarea } from '@chakra-ui/textarea';
import { isEqual } from 'lodash';
import React, { useEffect, useState } from 'react';
import useComments from '../../../hooks/comments/useComments';
import { CreateCommentType } from '../../../types/PostTypes';
import InputModal from '../InputModal';

interface CommentInputModalProps {
  initialCommentValue?: CreateCommentType;
  title: string;
  isOpen: boolean;
  onClose: () => void;
  type: 'add' | 'edit'
  postId?: number;
  commentId?: number;
}

const emptyCommentValue = {
  content: '',
  embed: '',
}

const CommentInputModal: React.FC<CommentInputModalProps> = (
    { initialCommentValue = emptyCommentValue, onClose, isOpen, title, type, commentId, postId }
  ) => {
  
  const initialState = { ...emptyCommentValue, ...initialCommentValue };
  const [commentValue, setCommentValue] = useState(initialState);
  const { addComment, editComment } = useComments();

  const closeModal = () => {
    onClose();
    setCommentValue(initialState)
  }

  const handleSubmit = (event: React.SyntheticEvent) => {
    event.preventDefault();
    closeModal();

    if (type === 'add') addComment(postId!, commentValue);
    if (type === 'edit') editComment(commentId!, commentValue);
  }

  const setContent = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setCommentValue(prev => ({ ...prev, content: event.target.value }))
  }

  const validateComment = (comment: CreateCommentType) => {
    if ((!comment.content && !comment.embed) || comment.content!.length < 2) return false;
    if (isEqual(comment, initialState)) return false;

    return true;
  }

  const ModalFooter: React.FC = () => {
    return <Button type="submit" disabled={!validateComment(commentValue)}>Submit</Button>;
  }

  return (
    <InputModal
      title={title}
      isOpen={isOpen}
      onClose={closeModal}
      footer={<ModalFooter />}
      handleSubmit={handleSubmit}
    >
      <Textarea rows={4} onChange={setContent} value={commentValue.content} />
    </InputModal>
  );
}

export default CommentInputModal;
