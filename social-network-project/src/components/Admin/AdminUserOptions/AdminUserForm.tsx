import React, { useState } from 'react';
import { FaTimesCircle, FaTrashAlt } from 'react-icons/fa';
import { useColorModeValue } from '@chakra-ui/color-mode';
import { banUser, deleteUser } from '../../../services/requests';
import useProfile from '../../../hooks/useProfile';
import {
  Divider,
  FormControl,
  FormLabel,
  IconButton,
  Textarea,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  VStack
} from '@chakra-ui/react';

const AdminUserForm: React.FC<{ userId: number }> = ({ userId }) => {
  const formBackground = useColorModeValue('gray.50', 'gray.800');
  const { token } = useProfile()
  const [ban, setBan] = useState({
    reason: '',
    period: 0
  });
  
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    banUser(userId, token, ban)
      .then(console.log);
  };

  const handleDelete = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    deleteUser(userId, token)
      .then(console.log);
    console.log('You deleted a person, this is irreversible!')
  };

  return (
    <VStack>
      <form className="user-ban-form" onSubmit={handleSubmit}>
        <FormControl>
          <FormLabel>Reason for ban</FormLabel>
          <Textarea
            bgColor={formBackground}
            value={ban.reason}
            onChange={(e) => setBan(prev => ({...prev, reason: e.target.value}))}
          />
        </FormControl>
        <FormControl>
          <FormLabel>Duration (days)</FormLabel>
          <NumberInput
            value={ban.period}
            onChange={(value: string) => setBan(prev => ({...prev, period: +value}))}
          >
            <NumberInputField
            />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
          <IconButton
            type="submit"
            aria-label="ban-user-button"
            my={2}
            w="full"
            icon={<FaTimesCircle />}
          />
        </FormControl>
        <Divider />
        <IconButton
          type="button"
          aria-label="delete-user-button"
          w="full"
          my={2}
          onClick={handleDelete}
          icon={<FaTrashAlt />}
        />
      </form>
    </VStack>
  );
};

export default AdminUserForm;
