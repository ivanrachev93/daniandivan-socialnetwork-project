import { Image } from '@chakra-ui/image';
import { AspectRatio } from '@chakra-ui/layout';
import React from 'react';
import { SERVER_ADDRESS } from '../../common/constants';

const Picture: React.FC<{ src: string }> = ({ src }) => {
  return (
    <AspectRatio w="full" ratio={16/9}>
      <Image src={`${SERVER_ADDRESS}/${src}`}/>
    </AspectRatio>
  )
}

export default Picture;
