import React from 'react';
import { Box, Container, Flex, HStack, Text } from '@chakra-ui/layout';
import { useColorModeValue } from '@chakra-ui/color-mode';
import { Link } from 'react-router-dom';

const Footer: React.FC = () => {
  const footerBgColor = useColorModeValue('#cbcbd3', 'gray.800')
  const textColor = useColorModeValue('gray.700', 'gray.300')

  return (
    <Box bg={footerBgColor}>
      <Container maxW="container.2xl" px={[4, 12]}>
      <Flex 
        color={textColor}
        fontSize={20}
        py={6}
        direction={['column', 'row']}
        align="center"
        justify="space-between"
      >
        <Text>© YourSpace 2021</Text>
        <HStack spacing={4}>
          <Link to="/about">About</Link>
        </HStack>
      </Flex>
      </Container>
    </Box>
  );
};

export default Footer;
