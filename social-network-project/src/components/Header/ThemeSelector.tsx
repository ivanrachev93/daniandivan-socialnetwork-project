import React from 'react';
import { useColorMode, useColorModeValue } from '@chakra-ui/color-mode';
import { IconButton } from '@chakra-ui/button';
import { FaSun, FaMoon } from 'react-icons/fa';

const ThemeSelector: React.FC = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  const buttonColor = useColorModeValue('gray.200', 'gray.500')
  const buttonHoverColor = useColorModeValue('gray.300', 'gray.400')

  return (
    <IconButton
      zIndex="3"
      aria-label="Toggle Theme" 
      bg="none"
      _hover={{background: 'none', color: buttonHoverColor}}
      _active={{background: 'none'}}
      p={0}
      m={0}
      color={buttonColor}
      onClick={toggleColorMode} 
      icon={colorMode === 'light' ? <FaMoon /> : <FaSun />} 
    />
  )
}

export default ThemeSelector;