import { Link } from 'react-router-dom';
import YourSpaceIcon from '../Icon/YourSpaceIcon';
import LoginButton from '../Buttons/LoginButton';
import UsersButton from '../Buttons/UsersNavButton';
import { FaUser } from 'react-icons/fa';
import LogoutButton from '../Buttons/LogoutButton';
import ThemeSelector from './ThemeSelector';
import useProfile from '../../hooks/useProfile';
import NotificationsPopover from '../Popover/NotificationsPopover';

import { ButtonGroup } from '@chakra-ui/button';
import {
  Container,
  Flex,
  Box,
  Menu,
  MenuItem,
  MenuButton,
  MenuList,
  Image,
  HStack,
} from '@chakra-ui/react';
import { useBreakpointValue } from '@chakra-ui/media-query';
import { useColorModeValue } from '@chakra-ui/color-mode';
import { Divider } from '@chakra-ui/layout';

const AccountMenu: React.FC = () => {
  const { avatar, username, id: myId } = useProfile();

  return (
    <Box display="flex" alignItems="center">
      <Menu direction="ltr">
        <MenuButton 
          fontSize="xl"
          _hover={{ background: useColorModeValue('gray.50', 'gray.700')}}
          _active={{ background: useColorModeValue('gray.50', 'gray.700')}}
          px={[2, 4]}
          py={[1, 2]}
          borderRadius="2em"
        >
          <HStack spacing={3}>
            <Image boxSize="1.5em" borderRadius="100%" src={avatar} />
            <span>{username}</span>
          </HStack>
        </MenuButton>
        <MenuList>
          <Link to={`/users/${myId}`}>
            <MenuItem paddingLeft={12} icon={<FaUser />}>
                My Account
            </MenuItem>
          </Link>
          <Divider />
          <Flex justifyContent="center" marginTop={2}>
            <LogoutButton />
          </Flex>
        </MenuList>
      </Menu>
    </Box>
  );
};

const Header: React.FC = () => {
  const { isLoggedIn } = useProfile();
  const buttonSize = useBreakpointValue(['sm', 'md', 'lg']);

  return (
    <Box
      bg={useColorModeValue('white', 'gray.800')}
      w="full"
      py={[2, 3]}
      pos="sticky"
      top="0"
      zIndex="99"
    >
      <Container maxW="container.xl">
        <Flex justifyContent="space-between" alignItems="center">
          <HStack spacing={[2, 6]}>
            <Link to="/home">
              <YourSpaceIcon />
            </Link>
            <UsersButton />
          </HStack>
          <ButtonGroup size={buttonSize} alignItems="center">
            <HStack spacing={2} h="2em">
              <ThemeSelector />
              <Divider orientation="vertical" />
              {isLoggedIn && <NotificationsPopover />}
              {isLoggedIn ? <AccountMenu /> : <LoginButton />}
            </HStack>
          </ButtonGroup>
        </Flex>
      </Container>
    </Box>
  );
};

export default Header;
