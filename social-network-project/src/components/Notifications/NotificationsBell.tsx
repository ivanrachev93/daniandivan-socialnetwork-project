import { useContext, useEffect, useState } from 'react';
import { FaBell } from 'react-icons/fa';
import { Box, Flex } from '@chakra-ui/layout';
import useProfile from '../../hooks/useProfile';
import { SimpleUserType } from '../../types/UserTypes';
import { extractFriendRequests } from '../../common/helpers';
import { useColorModeValue } from '@chakra-ui/react';
import { AuthContext } from '../../providers/AuthContext';

const NotificationsBell: React.FC = () => {
  const { friends } = useProfile();
  const [notifications, setNotifications] = useState<SimpleUserType[]>([]);
  const buttonHoverColor = useColorModeValue('gray.500', 'gray.400')
  const buttonColor = useColorModeValue('gray.200', 'gray.500')
  const { trigger } = useContext(AuthContext);

  useEffect(() => {
    const nots = extractFriendRequests(friends);
    setNotifications(() => nots);
  }, [friends, trigger]);

  return (
    <Box
      position="relative"
    >
      <Box as={FaBell} _hover={{ color: buttonHoverColor }} color={buttonColor} bg="none"/>
      {notifications.length > 0 && 
      <Flex
        zIndex="10"
        h="15px"
        w="15px"
        justifyContent="center"
        alignItems="center"
        bgColor="red"
        borderRadius="full"
        position="absolute"
        fontSize="0.65rem"
        fontWeight="ligther"
        top="50%"
        left="50%"
      >
        {notifications.length}
      </Flex>}
    </Box>
  );
};

export default NotificationsBell;
