import React, { useContext } from 'react';
import { Flex } from '@chakra-ui/layout';
import AcceptButton from '../Buttons/FriendshipButtons/AcceptButton';
import UsernameWithPic from '../CardComponents/UsernameWithPic/UsernameWithPic';
import { SimpleUserType } from '../../types/UserTypes';
import { AuthContext } from '../../providers/AuthContext';

interface NotificationsProps {
  friendRequests: SimpleUserType[];
}

const NotificationsContent: React.FC<NotificationsProps> = ({ friendRequests }) => {
  const { setTrigger } = useContext(AuthContext);

  return (
    <>
      {friendRequests.length > 0 
        ? friendRequests.map(user => (
          <Flex key={user.id}>
            <UsernameWithPic
              username={user.username}
              avatar={user.avatar}
              id={user.id}  
            />
            <AcceptButton 
              toId={user.id}
              setFrChanged={setTrigger}
            />
          </Flex>
        ))
        : 'No friend requests to show'}
    </>
  );
};

export default NotificationsContent;
