import { AspectRatio, Box, HStack, Text } from '@chakra-ui/layout';
import { Image } from '@chakra-ui/image';
import { SERVER_ADDRESS } from '../../../common/constants';
import { UserWithPicProps } from '../../../types/UserTypes';
import { Link } from 'react-router-dom';
import { useColorModeValue } from '@chakra-ui/react';

const UsernameWithPic: React.FC<UserWithPicProps> = ({ username, avatar, id, size }) => {
  const bgColor = useColorModeValue('rgba(0,0,0,0.05)', 'rgba(255,255,255,0.05)')
  const bgHoverColor = useColorModeValue('rgba(0,0,0,0.1)', 'rgba(255,255,255,0.1)')

  return (
    <Link to={`/users/${id}`}>
      <Box
        background={bgColor}
        _hover={{ background: bgHoverColor }}
        borderRadius="full"
        pr={4}
        transition="background .15s ease"
      >
        <HStack spacing={4} justifyContent="start" alignItems="center">
          <AspectRatio w={['2em', '2.75em']} ratio={1}>
            <Image
              objectFit="cover"
              borderRadius="full"
              alt={`${username}-${avatar || 'default.png'}`}
              src={`${SERVER_ADDRESS}/${avatar || 'default.png'}`}
            />
          </AspectRatio>
          <Text fontSize={['lg', '2xl']}>{username}</Text>
        </HStack>
      </Box>
    </Link>
  );
};

export default UsernameWithPic;
