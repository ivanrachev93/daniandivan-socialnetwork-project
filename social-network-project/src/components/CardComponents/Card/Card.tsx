import { useColorModeValue } from '@chakra-ui/color-mode';
import { Box } from '@chakra-ui/layout';

interface CardProps {
  type?: string;
  children: React.ReactNode;
};

const Card: React.FC<CardProps> = ({ type, children }) => {
  const formBackground = useColorModeValue('gray.50', 'gray.800');

  return (
    <Box
      w="full"
      backgroundColor={formBackground}
      borderRadius={3}
    >
      {children}
    </Box>
  );
};

export default Card;
