import React from 'react';

import { Input, FormControl, FormLabel } from '@chakra-ui/react';

interface PictureInputProps {
  formData: FormData;
  hideLabel?: boolean;
};

const PictureInput: React.FC<PictureInputProps> = ({ formData, hideLabel }) => {
  return (
    <FormControl>
      {!hideLabel && <FormLabel>Picture</FormLabel>}
      <Input
        type="file"
        onChange={(e) => formData.set('file', e.target.files![0])}
        py={[2.5, 1.5]}
        color="gray.500"
      />
    </FormControl>
  );
};

export default PictureInput;
