import React from 'react';
import {
  Input,
  FormControl,
} from '@chakra-ui/react';

interface SearchInputProps {
  searchTerm: string;
  setTerm: React.Dispatch<React.SetStateAction<string>>;
}

const SearchInput: React.FC<SearchInputProps> = ({
  searchTerm,
  setTerm
}) => {
  return (
    <FormControl>
      <Input
        type="text"
        w="full"
        value={searchTerm}
        onChange={(e) => setTerm(e.target.value)}
        placeholder="Search..."
      />
    </FormControl>
  );
};

export default SearchInput;
