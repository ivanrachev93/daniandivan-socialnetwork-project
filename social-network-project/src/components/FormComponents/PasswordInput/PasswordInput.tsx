import React, { useState } from 'react';
import { Credentials } from '../../../common/constants';
import { updateUserForm } from '../../../common/helpers';
import { UserObject } from '../../../types/UserFormTypes';

import {
  Button,
  FormControl,
  FormLabel,
  InputGroup,
  Input,
  InputRightElement,
  FormErrorMessage,
} from '@chakra-ui/react';
import { validatePassword } from '../../../common/validators';

interface PasswordInputProps {
  passwordUpdate?: boolean;
  password: string;
  isTouched: boolean;
  setPassword: React.Dispatch<React.SetStateAction<UserObject>>;
  formData?: FormData;
};

const PasswordInput: React.FC<PasswordInputProps> = ({
  passwordUpdate,
  password,
  isTouched,
  setPassword,
  formData
}) => {
  const [showPassword, togglePassword] = useState(false);
  const validPassword = validatePassword(password);

  return (
    <FormControl isInvalid={!validPassword && isTouched}>
      <FormLabel>{passwordUpdate ? 'New Password' : 'Password'}</FormLabel>
      {isTouched &&
      <FormErrorMessage>Username must be between 6 and 40 characters!</FormErrorMessage>}
      <InputGroup>
        <Input
          placeholder="pass1234"
          type={showPassword ? 'text' : 'password'}
          value={password}
          onChange={(e) => {
            updateUserForm(
              passwordUpdate 
                ? Credentials.NEW_PASSWORD
                : Credentials.PASSWORD,
              e.target.value,
              setPassword
            );
            formData?.set(
              passwordUpdate 
                ? Credentials.NEW_PASSWORD
                : Credentials.PASSWORD,
              e.target.value
            );
          }}
        />
        <InputRightElement w="4.5rem">
          <Button h="full" onClick={() => togglePassword(prev => !prev)}>
            {showPassword ? 'Hide' : 'Show'}
          </Button>
        </InputRightElement>
      </InputGroup>
    </FormControl>
  );
};

export default PasswordInput;