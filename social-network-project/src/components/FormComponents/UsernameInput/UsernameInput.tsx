import React from 'react';
import { Credentials } from '../../../common/constants';
import { updateUserForm } from '../../../common/helpers';
import { UserObject } from '../../../types/UserFormTypes';
import { validateUsername } from '../../../common/validators';

import { FormControl, FormLabel, Input, FormErrorMessage } from '@chakra-ui/react';

interface UsernameInputProps {
  username: string;
  isTouched: boolean;
  formData?: FormData;
  setUsername: React.Dispatch<React.SetStateAction<UserObject>>;
}

const UsernameInput: React.FC<UsernameInputProps> = ({
  username,
  isTouched,
  formData,
  setUsername
}) => {
  const validUsername = validateUsername(username);

  return (
    <FormControl isInvalid={!validUsername && isTouched}>
      <FormLabel>Username</FormLabel>
      {isTouched &&
      <FormErrorMessage>Username must be between 4 and 40 characters!</FormErrorMessage>}
      <Input
        placeholder="Username"
        value={username}
        onChange={(e) => {
          updateUserForm(Credentials.USERNAME, e.target.value, setUsername);
          formData?.set(Credentials.USERNAME, e.target.value);
        }}
      />
    </FormControl>
  );
};

export default UsernameInput;
