import React from 'react';
import { Credentials } from '../../../common/constants';
import { updateUserForm } from '../../../common/helpers';
import { UserObject } from '../../../types/UserFormTypes';

import { Input, FormControl, FormLabel, FormErrorMessage } from '@chakra-ui/react';
import { validateEmail } from '../../../common/validators';

interface EmailInputProps {
  email?: string;
  setEmail: React.Dispatch<React.SetStateAction<UserObject>>;
  formData?: FormData;
  isTouched?: boolean;
}

const EmailInput: React.FC<EmailInputProps> = ({ email, setEmail, isTouched, formData }) => {
  const validEmail = validateEmail(email!);

  return (
    <FormControl isInvalid={!validEmail && isTouched}>
      <FormLabel>Email</FormLabel>
      {isTouched && 
      <FormErrorMessage>You must provide a valid email expression!</FormErrorMessage>}
      <Input
        placeholder="generic.email@email.com"
        value={email}
        onChange={(e) => {
          updateUserForm(Credentials.EMAIL, e.target.value, setEmail);
          formData?.set(Credentials.EMAIL, e.target.value);
        }}
      />
    </FormControl>
  );
};

export default EmailInput;
