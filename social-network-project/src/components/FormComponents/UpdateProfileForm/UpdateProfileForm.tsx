import { Button } from '@chakra-ui/button';
import { VStack } from '@chakra-ui/layout';
import React, { useState, useRef } from 'react';
import useProfile from '../../../hooks/useProfile';
import { sendProfileUpdateRequest } from '../../../services/requests';
import { UserObject } from '../../../types/UserFormTypes';
import PictureInput from '../PictureInput/PictureInput';
import EmailInput from '../EmailInput/EmailInput';
import PasswordInput from '../PasswordInput/PasswordInput';
import UsernameInput from '../UsernameInput/UsernameInput';

const initialUser: UserObject = {
  username: {
    touched: false,
    value: '',
  },
  password: {
    touched: false,
    value: '',
  },
  newPassword: {
    touched: false,
    value: '',
  },
  email: {
    touched: false,
    value: '',
  }
};

const UpdateProfileForm: React.FC = () => {
  const [userCredentials, setUserCredentials] = useState(initialUser);
  const { token } = useProfile();
  const { current: formData } = useRef(new FormData());
  
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    // event.preventDefault();

    sendProfileUpdateRequest(formData, token)
      .then(console.log);
  };
  
  return (
    <form className="profile-update-form" onSubmit={handleSubmit}>
      <VStack spacing={5}>
        <UsernameInput
          username={userCredentials.username.value}
          isTouched={userCredentials.username.touched!}
          setUsername={setUserCredentials}
          formData={formData}
        />
        <PasswordInput
          password={userCredentials.password.value}
          isTouched={userCredentials.password.touched!}
          setPassword={setUserCredentials}
          formData={formData}
        />
        <PasswordInput
          passwordUpdate={true}
          password={userCredentials.newPassword?.value!}
          isTouched={userCredentials.newPassword?.touched!}
          setPassword={setUserCredentials}
          formData={formData}
        />
        <EmailInput
          email={userCredentials.email?.value}
          setEmail={setUserCredentials}
          formData={formData}
        />
        <PictureInput formData={formData} />
        <Button type="submit" w="full" py={[4, 6]} fontSize={['xl', '2xl']}>
          Submit Update
        </Button>
      </VStack>
    </form>
  );
};

export default UpdateProfileForm;
