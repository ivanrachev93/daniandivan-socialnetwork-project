import { Image } from '@chakra-ui/react';

const YourSpaceIcon: React.FC = () => {
  return (
    <Image alt="yourspace-icon" h="auto" w={[10, 14]} style={{ transform: 'scaleX(-1)' }} src="/yourSpace.svg" />
  );
};

export default YourSpaceIcon;