import { useColorModeValue } from '@chakra-ui/color-mode';
import { Box } from '@chakra-ui/layout';
import React from 'react';

interface SidebarProps {
  children: React.ReactNode
}

const Sidebar: React.FC<SidebarProps> = ({ children }) => {
  const bgColor = useColorModeValue('gray.50' ,'gray.800')

  return (
    <Box 
      position={['unset', null, 'sticky']}
      top="7em"
      w="100%"
      bg={bgColor}
      p={4}
      borderRadius={4}
    >
      {children}
    </Box>
  )
}


export default Sidebar;
