import React from 'react';
import usePublicFeed from '../../../hooks/feed/usePublicFeed';
import PostSimple from '../../Post/PostSimple/PostSimple';

const PublicFeed: React.FC = () => {
  const feed = usePublicFeed();

  if (feed.isError) return <>{feed.error.message}</>
  return (
    <>
      {feed.isSuccess && feed.data.map((post) => (
        <PostSimple key={post.id} post={post} />
      ))}
    </>
  );
};

export default PublicFeed;
