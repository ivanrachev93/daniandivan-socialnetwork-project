import React, { useState } from 'react';
import PostDetailed from '../../Post/PostDetailed/PostDetailed';
import usePrivateFeed from '../../../hooks/feed/usePrivateFeed';

import { Divider } from '@chakra-ui/layout';
import CreatePost from '../../InputModals/Post/CreatePost';
import { Button, HStack, Text } from '@chakra-ui/react';

const PrivateFeed: React.FC = () => {
  const [page, setPage] = useState(0);
  const count = 4;
  const privateFeed = usePrivateFeed(page, count);

  if (privateFeed.isError) return <>{privateFeed.error.message}</>;
  return (
    <>
      <CreatePost />
      <Divider />
      {privateFeed.isSuccess &&
        privateFeed.data.map((post) => (
          <PostDetailed key={post.id} post={post} />
        ))}
      {privateFeed.isSuccess && (
        <HStack spacing={4} justify={['stretch', 'end']}>
          <Button
            flexGrow={[1, 'unset']}
            disabled={page === 0}
            onClick={() => setPage((prev) => prev - 1)}
            fontSize={['lg', 'xl', '2xl']}
          >
            Prev
          </Button>
          <Text>{page + 1}</Text>
          <Button
            flexGrow={[1, 'unset']}
            disabled={privateFeed.data.length < count}
            onClick={() => setPage((prev) => prev + 1)}
            fontSize={['lg', 'xl', '2xl']}
          >
            Next
          </Button>
        </HStack>
      )}
    </>
  );
};

export default PrivateFeed;
