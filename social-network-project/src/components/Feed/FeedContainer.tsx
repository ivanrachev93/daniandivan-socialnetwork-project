import { VStack } from '@chakra-ui/react';
import PrivateFeed from './PrivateFeed/PrivateFeed';
import PublicFeed from './PublicFeed/PublicFeed';
import useProfile from '../../hooks/useProfile';

const FeedContainer: React.FC = () => {
  const { isLoggedIn } = useProfile();

  return (
    <VStack spacing={[4, 8]} align="stretch">
      {isLoggedIn ? <PrivateFeed /> : <PublicFeed />}
    </VStack>
  );
};

export default FeedContainer;
