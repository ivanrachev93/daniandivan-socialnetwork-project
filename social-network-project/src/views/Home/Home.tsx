import React from 'react';

import { Container, Box, Grid, VStack, Text, Divider } from '@chakra-ui/layout';
import Sidebar from '../../components/Sidebar/Sidebar';
import FeedContainer from '../../components/Feed/FeedContainer';
import WelcomeSection from './WelcomeSection'
import useProfile from '../../hooks/useProfile';
import UsernameWithPic from '../../components/CardComponents/UsernameWithPic/UsernameWithPic';

const Home: React.FC = () => {
  const { isLoggedIn, friends } = useProfile();

  return (
    <Box w="full" position="relative">
      {!isLoggedIn && <WelcomeSection />}
      <Container maxW="container.lg" py={[12, null, 24]}>
        <Grid
          templateColumns={['1fr', null, '1fr 2fr']}
          gap={[2, 4]}
          maxW="container.xl"
          alignItems="start"
        >
          <VStack order={[2, null, 1]} spacing={[2, 6]} position={['unset', null, 'sticky']} top="7em">
            <Sidebar>
              <Text pb={4} fontWeight="bold" fontSize="2xl">Fellow Music Lovers</Text>
              <Divider />
              <VStack pt={4} align="stretch" spacing={4}>
                {Array.isArray(friends) && friends.map(fr => (
                  <UsernameWithPic key={fr.id} username={fr.username} avatar={fr.avatar} id={fr.id} />
                ))}
              </VStack>
            </Sidebar>
          </VStack>
          <Box order={[1, null, 2]}>
            <FeedContainer />
          </Box>
        </Grid>
      </Container>
    </Box>
  );
};

export default Home;
