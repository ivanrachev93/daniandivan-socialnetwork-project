import React from 'react';

import { Container, Box, Heading, Text } from '@chakra-ui/layout';
import { ButtonGroup } from '@chakra-ui/button';
import { useBreakpointValue } from '@chakra-ui/media-query';
import { DarkMode, useColorModeValue } from '@chakra-ui/color-mode';

import LoginButton from '../../components/Buttons/LoginButton';
import { ButtonVariants } from '../../common/constants';
import SignUpButton from '../../components/Buttons/SignUpButton';

import heroImg from '../../images/hero-img.png';
import heroShapesDark from './hero-shapes-dark.svg';
import heroShapesWhite from './hero-shapes-white.svg';

const Home: React.FC = () => {
  const buttonSizes = useBreakpointValue(['sm', 'md', 'lg']);
  const shapes = useColorModeValue(heroShapesWhite, heroShapesDark)
  const bgImgTint = useColorModeValue('#7c7c7c4e', '#00000089')

  return (
    <Container
      px={0}
      bgImg={`linear-gradient(${bgImgTint},${bgImgTint}), url('${heroImg}')`}
      bgPos="center center"
      maxW="full"
      bgSize="cover"
    >
      <Container py={[16, 24]} maxW="full" bgImg={shapes} bgPos="center center" bgSize="cover">
        <Box px={2} textAlign={['unset', 'center']} color="white">
          <DarkMode>
            <Heading fontSize={['3xl', null, '5xl']} fontWeight="800">Welcome to Your Musical World</Heading>
            <Container maxW="36em" p={0} mt={2} mb={8}>
              <Text fontSize={['lg', 'xl', '2xl']} fontWeight="300">
                Form connections with people you love & discover amazing artists.
              </Text>
            </Container>
          </DarkMode>
            <ButtonGroup size={buttonSizes} flexDirection="row">
              <SignUpButton />
              <DarkMode><LoginButton variant={ButtonVariants.OUTLINE}/></DarkMode>
            </ButtonGroup>
        </Box>
      </Container >
    </Container>
  );
};

export default Home;
