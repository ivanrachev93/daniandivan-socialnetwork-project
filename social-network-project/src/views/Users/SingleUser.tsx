import { useContext } from 'react';
import { useLocation } from 'react-router';
import { useQuery } from 'react-query';
import { getPostsByUser, getUserById } from '../../services/requests';
import { evaluateFriendship } from '../../common/helpers';
import useProfile from '../../hooks/useProfile';
import UsernameWithPic from '../../components/CardComponents/UsernameWithPic/UsernameWithPic';
import { UserType } from '../../types/UserTypes';
import { PostDetailedType } from '../../types/PostTypes';
import FriendshipButton from '../../components/Buttons/FriendshipButtons/FriendshipButton';
import UpdateProfileButton from '../../components/Buttons/UpdateProfileButton';
import ProfileUpdateModal from '../../components/InputModals/ProfileUpdate/ProfileUpdateModal';

import { useColorModeValue } from '@chakra-ui/color-mode';
import {
  Box,
  VStack,
  Flex,
  Text,
  Stack,
  Divider,
  HStack,
} from '@chakra-ui/react';
import { useDisclosure } from '@chakra-ui/hooks';
import PostDetailed from '../../components/Post/PostDetailed/PostDetailed';
import { FaEnvelope, FaUser } from 'react-icons/fa';
import { AuthContext } from '../../providers/AuthContext';

const SingleUser: React.FC = () => {
  const formBackground = useColorModeValue('gray.50', 'gray.800');
  const { setTrigger } = useContext(AuthContext);
  const { pathname: location } = useLocation();
  const { id: myId, token } = useProfile();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const userId = location.split('/')[location.split('/').length - 1];
  const user = useQuery<UserType, Error>(
    `/users/${userId}`,
    () => getUserById(+userId, token),
    {
      refetchInterval: 5000,
    }
  );
  const posts = useQuery<PostDetailedType[], Error>(
    ['/users', userId, '/posts'],
    () => getPostsByUser(+userId, token)
  );
  const meAsAFriend =
    user.data?.friends.length! > 0
      ? user.data?.friends.find(({ id }) => myId === id) || null
      : null;
  const friendshipEvaluation = evaluateFriendship(
    meAsAFriend?.friendshipStatus!,
    meAsAFriend?.canAcceptFriendship!
  );

  const NoPostsFallback: React.FC = () => {
    return (
      <Flex p={4} align="center" justify="center" bg={formBackground} w="full" h="6em">
        <Text fontSize="xl" opacity="0.5">Shhh... nothing to see here.</Text>
      </Flex>
    )
  }

  return (
    <VStack w="container.lg" align="stretch" py={12} px={6} spacing={[2, 6]}>
      <Box borderRadius={4} backgroundColor={formBackground} px={6}>
        {user.isSuccess && (
          <Flex py={4} justify="space-between" alignItems="center">
            <UsernameWithPic
              id={user.data.id}
              username={user.data.username}
              avatar={user.data.avatar!}
            />
            <ProfileUpdateModal
              title="Update Profile"
              isOpen={isOpen}
              onClose={onClose}
            />
            {myId !== user.data.id ? (
              <FriendshipButton
                toId={user.data.id}
                areWeConnected={!!meAsAFriend}
                friendshipStatus={friendshipEvaluation}
                setFrChanged={setTrigger}
              />
            ) : (
              <UpdateProfileButton toggle={onOpen} />
            )}
          </Flex>
        )}
      </Box>

      <Stack
        direction={['column', null, 'row']}
        spacing={[3, 6]}
        align="start"
        justify="space-between"
      >
        <Box w={['100%', null, '35%']} mt={[4, null, 0]} order={[1, null, 1]}>
          <VStack spacing={4}>
            <Box backgroundColor={formBackground} w="full" p={6}>
              <Text borderColor="darkgrey">User information</Text>
              <Divider h={2}/>
              <VStack spacing={2} align="start" mt={3}>
                <HStack>
                  <FaEnvelope /><Text fontSize="xl">{user.data?.email}</Text>
                </HStack>
                <HStack>
                  <FaUser /><Text fontSize="xl">{user.data?.role === 2 ? 'Admin' : 'User'}</Text>
                </HStack>
              </VStack>
            </Box>

            <Box backgroundColor={formBackground} justifyItems="center" w="full" p={6}>
              <Text fontSize="2xl">Fellow music lovers</Text>
              <Divider h={2}/>
              <VStack spacing={4} mt={4} align="start">
                {user.data?.friends.map((friend) => (
                  <UsernameWithPic
                    id={friend.id}
                    key={friend.id}
                    username={friend.username}
                    avatar={friend.avatar!}
                  />
                ))}
              </VStack>
            </Box>
          </VStack>
        </Box>
        <Box w={['100%', null, '65%']} order={[1, null, 2]}>
          <VStack spacing={4}>
            {posts.isSuccess &&
              posts.data.length > 0
              ? posts.data.map((post) => (<PostDetailed key={post.id} post={post} />))
              : <NoPostsFallback />
            }
          </VStack>
        </Box>
      </Stack>
    </VStack>
  );
};

export default SingleUser;
