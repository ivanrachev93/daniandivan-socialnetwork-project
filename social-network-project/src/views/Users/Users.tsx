import React, { useContext, useState } from 'react';
import SearchInput from '../../components/FormComponents/SearchInput/SearchInput';
import { UserType } from '../../types/UserTypes';
import Card from '../../components/CardComponents/Card/Card';
import UsernameWithPic from '../../components/CardComponents/UsernameWithPic/UsernameWithPic';
import useProfile from '../../hooks/useProfile';
import { evaluateFriendship } from '../../common/helpers';
import FriendshipButton from '../../components/Buttons/FriendshipButtons/FriendshipButton';
import AdminPopover from '../../components/Popover/AdminPopover';
import { HStack, Stack, VStack } from '@chakra-ui/layout';
import { AuthContext } from '../../providers/AuthContext';
import useUsers from '../../hooks/users/useUsers';

import {
  Box,
  Divider,
  Text,
  Flex,
  CheckboxGroup,
  Checkbox,
  Grid,
  GridItem,
  useColorModeValue,
} from '@chakra-ui/react';
import { FaEnvelope } from 'react-icons/fa';

const Users: React.FC = () => {
  const formBackground = useColorModeValue('gray.50', 'gray.800');
  const { id: myId, isAdmin, isLoggedIn } = useProfile();
  const { setTrigger } = useContext(AuthContext);
  const [searchTerm, setSearchTerm] = useState('');
  const [filters, setFilters] = useState({
    byUsername: true,
    byEmail: false,
  });

  const allUsers = useUsers(
    filters.byUsername ? searchTerm : '',
    filters.byEmail ? searchTerm : ''
  );
  
  const handleSearch = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
  };
  
  return (
    <Stack
      spacing={12}
      direction="column"
      justify="start"
      w="full"
      m={2}
      py={8}
      maxW="container.lg"
    >
      <Flex
        justifyContent="center"
        alignItems="center"
        bg={formBackground}
        maxW="full"
        p={8} borderRadius={2}
      >
        <Box w="full">
        <form onSubmit={handleSearch}>
          <VStack spacing={4} align="start">
            <Box>
            <CheckboxGroup>
              <Checkbox
                isChecked={filters.byUsername}
                px={2}
                onChange={() => setFilters(prev => ({...prev, byUsername: !filters.byUsername}))}
              >
                by Username
              </Checkbox>
              <Checkbox
                isChecked={filters.byEmail}
                px={2}
                onChange={() => setFilters(prev => ({...prev, byEmail: !filters.byEmail}))}
              >
                by Email
              </Checkbox>
            </CheckboxGroup>
            </Box>
          <SearchInput
            searchTerm={searchTerm}
            setTerm={setSearchTerm}
          />
          </VStack>
        </form>
        </Box>
      </Flex>

      <Grid
        direction={['column', null, 'row']}
        templateColumns={['1fr', null, 'repeat(2, minmax(20px, 1fr))']}
        w="full"
        gap={[2, 4, 8]}
      >
        {allUsers.isSuccess && allUsers.data.map((user: UserType) => {
          const meAsAFriend = user.friends.length > 0 
            ? (user.friends.find(({ id }) => myId === id) || null)
            : null;
          const frienshipEvaluation = evaluateFriendship(
            meAsAFriend?.friendshipStatus!,
            meAsAFriend?.canAcceptFriendship!
          );
          
          return (
            <GridItem key={user.username} w="full">
              <Card>
                <Stack
                  direction={['row']}
                  justify="space-between"
                  align="center"
                  py={4}
                  px={[4, 8]}
                >
                  <HStack>
                    <UsernameWithPic
                      id={user.id}
                      username={user.username}
                      avatar={user.avatar!}
                    />

                  </HStack>
                  <Stack direction="row" alignItems="center">
                    {isAdmin && <AdminPopover userId={user.id} />}
                    {(isLoggedIn && myId !== user.id) &&
                    <FriendshipButton
                      toId={user.id}
                      areWeConnected={!!meAsAFriend}
                      friendshipStatus={frienshipEvaluation}
                      setFrChanged={setTrigger}
                    />}
                  </Stack>
                </Stack>                
                <Divider />
                <HStack justify="center" align="center" px={8} py={3} w="full">
                  <FaEnvelope /> <Text>{user.email}</Text>
                </HStack>
              </Card>
            </GridItem>
          );
        })}
      </Grid>
    </Stack>
  );
};

export default Users;
