import { Flex, Text } from '@chakra-ui/react';
import Embed from '../../components/Embed/YoutubeEmbed';

const PageNotFound: React.FC = () => {
  return (
    <Flex
      justifyContent="center"
      alignItems="center"
      flexDirection="column"
    >
      <Text
        fontSize="3rem"
      >
        You seem lost...
      </Text>
      <Text
        fontSize="3rem"
      >
        Is this what you're looking for?
      </Text>
      <br />
      <Embed src="dQw4w9WgXcQ?autoplay=1" />
    </Flex>
  );
};

export default PageNotFound;