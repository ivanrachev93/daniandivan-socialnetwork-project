import { Button, ButtonGroup } from '@chakra-ui/button';
import { Container, Divider, VStack, Text, Flex, Box, Stack } from '@chakra-ui/layout';
import React, { Fragment, useEffect, useState } from 'react';
import Login from '../../components/Login/Login';
import { useColorModeValue } from '@chakra-ui/color-mode';
import { useBreakpointValue } from '@chakra-ui/media-query';
import { useLocation } from 'react-router-dom';
import Register from '../../components/Register/Register';
import { Link } from 'react-router-dom';
import { FaHeart, FaMusic } from 'react-icons/fa'
import IconBox from './IconBox'

type LinkType = '/login' | '/register';

interface INav {
  key: number,
  title: string,
  link: LinkType,
  active: boolean,
}

interface NavButtonsProps {
  navItems: INav[],
}

const NavButtons: React.FC<NavButtonsProps> = ({ navItems }) => {
  const inactiveText = useColorModeValue('gray.900', 'gray.50');
  const inactiveBg = useColorModeValue('gray.200', 'gray.700')
  const inactiveBgHover = useColorModeValue('gray.300', 'gray.600')

  return (
    <Fragment>
      {navItems.map(btn => {
        if (btn.active === false) {
          return (
            <Link key={btn.key} to={btn.link}>
              <Button
                color={inactiveText}
                bg={inactiveBg}
                opacity=".5"
                _hover={{
                  background: inactiveBgHover
                }}
              >
                {btn.title}
              </Button>
            </Link>
          )
        }

        return (
          <Button key={btn.key} pointerEvents="none">{btn.title}</Button>
        )
      })}
    </Fragment>
  )
}

const LoginRegister: React.FC = () => {
  const { pathname: activeNav } = useLocation();
  const [navItems, setNavItems] = useState<INav[]>([
    { key: 1, title: 'Log In', link: '/login', active: false },
    { key: 2, title: 'Register', link: '/register', active: false },
  ])

  const bgColor= useColorModeValue('gray.100', 'gray.900')
  const buttonSizes = useBreakpointValue(['md', 'lg', '3xl']);
  const iconColor = useColorModeValue('blue.400', 'red.400');
  const formBackground = useColorModeValue('gray.50', 'gray.800');

  useEffect(() => {
    setNavItems(prev => {
      return prev.map(el => {
        if (el.link === activeNav) return { ...el, active: true }
        return { ...el, active: false }
      })
    })
  }, [activeNav])

  return (
    <Box w="full" bg={bgColor}>
      <Container maxW="container.lg">
        <Stack
          direction={['column', null, 'row']}
          align="center"
          justify="start"
          spacing={[10, 16, 24]}
          minH={['unset', '90vh']}
          py={12}
        >
          <VStack spacing={[4, 8]} w={['100%', null, '50%']} alignItems="start">
            <Flex direction="row" w="full">
            <ButtonGroup size={buttonSizes}>
              <NavButtons navItems={navItems} />
            </ButtonGroup>
            </Flex>
            <Divider />
            <Text fontWeight="light" fontSize={['xl', null, '2xl']}>
              Become a part of our awesome music-loving community.
            </Text>
            <VStack spacing={4} w="full">
              <IconBox text="Discover amazing artists.">
                <Box as={FaMusic} color={iconColor} fontSize={[16, 24]} />
              </IconBox>
              <IconBox text="Meet fellow music lovers nearby.">
                <Box as={FaHeart} color={iconColor} fontSize={[16, 24]} />
              </IconBox>
            </VStack>
          </VStack>
          <Box bg={formBackground} w={['100%', null, '50%']} p={8} borderRadius={4}>
            {activeNav === '/login' ? <Login /> : <Register />}
          </Box>
        </Stack>
      </Container>
    </Box>
  )
}

export default LoginRegister;
