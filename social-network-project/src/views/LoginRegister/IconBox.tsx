import React from 'react';
import { Flex, Box, Text } from '@chakra-ui/layout'
import { useColorModeValue } from '@chakra-ui/color-mode';

interface IconBoxProps {
  text: string,
  children: React.ReactNode
}

const IconBox: React.FC<IconBoxProps> = ({ text, children }) => {
  const boxColor = useColorModeValue('gray.50', 'gray.800');

  return (
    <Flex
      direction="row"
      borderRadius={4}
      overflow="hidden"
      bg={boxColor}
      align="center"
      paddingRight={4}
      w="full"
    >
      <Box p={4}>
        {children}
      </Box>
      <Text fontSize={['sm', 'md', 'xl']}>{text}</Text>
    </Flex>
  )
}

export default IconBox;