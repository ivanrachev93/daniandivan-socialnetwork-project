import React from 'react';
import { Box, Text, HStack } from '@chakra-ui/layout' 
import { Avatar, Heading, Image, VStack, Flex, useColorModeValue } from '@chakra-ui/react';
import chakraUiImg from '../../images/chakra-ui.png'
import daniImg from '../../images/danipfpcrop.jpeg'
import vankaImg from '../../images/Vanka.jpg';
import reactImg from '../../images/react-logo.png'
import { FaGitlab } from 'react-icons/fa';

const PersonBox: React.FC<
  { heading: string, text: string, src: string, git: string}
> = ({ heading, text, src, git }) => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'gray.50');

  return (
    <Flex bg={bgColor} color={textColor} px={7} py={4} borderRadius={4} h="12em" w="33em" alignItems="center">
      <HStack spacing={6}>
        <Avatar src={src} size="xl"/>
        <Box>
          <Heading>{heading}</Heading>
          <Text fontWeight="light" mb={4}>{text}</Text>
          <HStack><FaGitlab color="orange" /><a href={git}>{git}</a></HStack>
        </Box>
      </HStack>
    </Flex>
  )
}

const IconBox: React.FC<{ src: string }> = ({ src }) => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  return (
    <Box bg={bgColor} p={4} borderRadius={4} h="10em" display="flex" alignItems="center">
      <Image src={src} w="7em"/>
    </Box>
  )
}

const About: React.FC = () => {
  return (
    <Flex minH="86vh" alignItems="center">
      <VStack spacing={12} py={12} maxW="container.lg">
        <Box>
          <Box mb={8}>
            <Heading mb={2} fontWeight="extrabold" textAlign="center" fontSize="5xl">
              Did you like the UX with our app?
            </Heading>
            <Text textAlign="center" fontSize="2xl" fontWeight="light">
              These are the minds behind the curtains.
            </Text>
          </Box>
          <HStack spacing={12}>
            <PersonBox
              heading="Ivan Rachev"
              text="System is loading a web developer, please wait!"
              git="https://gitlab.com/ivanrachev93"
              src={vankaImg}
            />
            <PersonBox
              heading="Daniel Dimitrov"
              text="Why are you reading this? Pay attention to the presentation!"
              git="https://gitlab.com/dan1rd"
              src={daniImg}
            />
          </HStack>
        </Box>
        <Heading mb={2} fontWeight="extrabold" textAlign="center" fontSize="5xl">
          What did we use, you might ask?
        </Heading>
        <HStack spacing={8}>
          <IconBox src="./ts-logo.svg" />
          <IconBox src={reactImg} />
          <IconBox src="./react-query-logo.svg" />
          <IconBox src={chakraUiImg} />
        </HStack>
      </VStack>
    </Flex>
  )
}

export default About;
