import { SimpleUserType } from './UserTypes';

export interface UserContextType {
  username: string;
  id: number;
  avatar?: string | null | undefined;
  token: string;
  role: number;
  email: string;
  isLoggedIn: boolean;
  exp?: number;
  iat?: number;
  friends: SimpleUserType[]
};

export interface AuthContextType {
  user: UserContextType;
  setAuth: React.Dispatch<React.SetStateAction<UserContextType>>;
  trigger: boolean;
  setTrigger: React.Dispatch<React.SetStateAction<boolean>>;
};
