type InputField = {
  value: string,
  touched?: boolean,
};

export interface UserObject {
  username: InputField,
  password: InputField,
  newPassword?: InputField,
  email?: InputField,
};
