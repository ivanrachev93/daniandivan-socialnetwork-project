import { SimpleUserType } from './UserTypes';

export interface PostSimpleType {
  id: number;
  content: string;
  picture?: string;
  embed?: string;
  createdOn: string;
  likesCount: number;
}

export interface ReactionsType extends SimpleUserType {
  canAcceptFriendship: boolean;
  reaction: number;
}

export interface CommentType extends PostSimpleType {
  updatedOn: string;
  author: SimpleUserType;
  likes: ReactionsType[];
}

export interface CreateCommentType {
  content: string;
  embed: string;
}

export interface PostSimpleType {
  id: number;
  content: string;
  picture?: string;
  embed?: string;
  createdOn: string;
  likesCount: number;
}

export interface PostDetailedType extends PostSimpleType {
  updatedOn: string;
  isPublic: boolean;
  longitude: string;
  latitude: string;
  author: SimpleUserType;
  likes: ReactionsType[];
  comments: CommentType[];
}

export interface CreatePostType {
  content?: string,
  embed?: string,
  latitude?: string,
  longitude?: string,
  isPublic: boolean;
  picture: string | Blob;
}

export interface PostResponseType {
  id:	number
  content: string
  picture: string
  embed: string
  latitude: number
  longitude: number
  createdOn: string
  updatedOn: string
  isPublic: boolean
}

export interface ReactionType {
  reaction: number;
}
