export interface SimpleUserType {
  id: number;
  username: string;
  avatar?: string;
  friendshipStatus: number;
  canAcceptFriendship: boolean;
}

export interface UserType {
  avatar: string | null;
  banDate: string | Date | null;
  banReason: string | null;
  email: string;
  friends: SimpleUserType[];
  id: number;
  lastUpdated: string | Date;
  latitude: number | null;
  longitude: number | null;
  role: number;
  username: string;
};

export interface UserWithPicProps {
  username: string;
  avatar?: string;
  size?: number;
  id: number;
};
