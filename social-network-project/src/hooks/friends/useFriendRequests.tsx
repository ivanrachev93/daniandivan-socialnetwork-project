import { extractFriendRequests } from '../../common/helpers';
import useProfile from '../useProfile';

const useFriendRequests = () => {
  const { friends } = useProfile();
  const friendReqs = extractFriendRequests(friends);

  return friendReqs;
};

export default useFriendRequests;
