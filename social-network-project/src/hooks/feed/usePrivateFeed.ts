import useProfile from '../useProfile';
import { useQuery } from 'react-query';
import { PostDetailedType } from '../../types/PostTypes';
import { getPrivateFeed } from '../../services/requests';

const usePrivateFeed = (page: number = 0, count: number = 4, distance?: string) => {
  const { token } = useProfile();
  return useQuery<PostDetailedType[], Error>(
    ['/feed', page, count, distance],
    () => getPrivateFeed(token, page, count, distance),
    {
      refetchInterval: 1000
    }
  );
};

export default usePrivateFeed;
