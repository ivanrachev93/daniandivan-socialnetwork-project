import { useQuery } from 'react-query';
import { PostSimpleType } from '../../types/PostTypes';
import { getPublicFeed } from '../../services/requests';

const usePublicFeed = () => {
  return useQuery<PostSimpleType[], Error>(
    '/feed/popular', getPublicFeed
  );
};

export default usePublicFeed;
