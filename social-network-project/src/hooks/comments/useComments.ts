import { useMutation, useQueryClient } from 'react-query'
import { addCommentRequest, deleteCommentRequest, editCommentRequest } from '../../services/requests'
import { CommentType, CreateCommentType } from '../../types/PostTypes'
import useProfile from '../useProfile'

export type useCommentsReturnType = {
  addComment: (postId: number, body: CreateCommentType) => void;
  editComment: (commentId: number, body: CreateCommentType) => void;
  deleteComment: (commentId: number) => void;
}

const useComments = () => {
  const { token } = useProfile()
  const queryClient = useQueryClient();

  interface AddCommentVariables {
    postId: number;
    body: CreateCommentType;
  }

  const addCommentMutation = useMutation<CommentType, Error, AddCommentVariables>(
    ({ postId, body }) => addCommentRequest(postId, body, token)
  )

  const addComment = (postId: number, body: CreateCommentType) => {
    addCommentMutation.mutate(({ postId, body }), {
      onSuccess: () => {
        queryClient.invalidateQueries('/feed');
        queryClient.invalidateQueries('/users');
      },
      onError: () => {
        console.log('something went wrong') // Todo
      },
    })
  }

  interface EditCommentVariables {
    commentId: number;
    body: CreateCommentType;
  }

  const editCommentMutation = useMutation<CommentType, Error, EditCommentVariables>(
    ({ commentId, body }) => editCommentRequest(commentId, body, token)
  )

  const editComment = (commentId: number, body: CreateCommentType) => {
    editCommentMutation.mutate(({ commentId, body }), {
      onSuccess: () => {
        queryClient.invalidateQueries('/feed');
        queryClient.invalidateQueries('/users');
      },
      onError: () => {
        console.log('something went wrong') // Todo
      },
    })
  }

  const deleteCommentMutation = useMutation<CommentType, Error, number>(
    commentId => deleteCommentRequest(commentId, token)
  )

  const deleteComment = (commentId: number) => {
    deleteCommentMutation.mutate(commentId, {
      onSuccess: () => {
        queryClient.invalidateQueries('/feed');
        queryClient.invalidateQueries('/users');
      },
      onError: () => {
        console.log('something went wrong') // Todo
      },
    })
  }

  return { addComment, editComment, deleteComment };
}

export default useComments;
