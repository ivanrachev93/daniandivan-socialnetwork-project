import { useQuery } from 'react-query';
import { getUsers } from '../../services/requests';

const useUsers = (username: string, email: string) => {
  return useQuery(
    ['/users', username, email],
    () => getUsers(username, email),
    {
      refetchInterval: 3000
    }
  );
};

export default useUsers;