import { useMutation, useQueryClient } from 'react-query';
import { addPostRequest, deletePostRequest, editPostRequest, MessageResponse } from '../../services/requests';
import { PostResponseType } from '../../types/PostTypes';
import useProfile from '../useProfile';

export type usePostsReturnType = {
  addPost: (newPost: FormData) => void;
  deletePost: (postId: number) => void;
  editPost: (postId: number, body: FormData) => void;
}

const usePosts = (): usePostsReturnType => {
  const { token } = useProfile()
  const queryClient = useQueryClient();
  
  const addPostMutation = useMutation<PostResponseType, Error, FormData>(
    newPost => addPostRequest(token, newPost)
  )
  
  const addPost = (newPost: FormData) => {
    addPostMutation.mutate(newPost, {
      onSuccess: () => {
        queryClient.invalidateQueries('/feed');
      },
      onError: (error) => {
        console.log('something went wrong:' + error) // Todo
      },
    })
  }

  const deletePostMutation = useMutation<MessageResponse, Error, number>(
    postId => deletePostRequest(token, postId)
  )

  const deletePost = (postId: number) => {
    deletePostMutation.mutate(postId, {
      onSuccess: () => {
        queryClient.invalidateQueries('/feed');
        queryClient.invalidateQueries('/users');
      },
      onError: () => {
        console.log('something went wrong') // Todo
      } 
    })
  }

  interface EditPostVariables {
    postId: number;
    body: FormData;
  }

  const editPostMutation = useMutation<PostResponseType, Error, EditPostVariables>(
    ({ postId, body })  => editPostRequest(token, postId, body)
  )

  const editPost = (postId: number, body: FormData) => {
    editPostMutation.mutate({ postId, body }, {
      onSuccess: () => {
        queryClient.invalidateQueries('/feed');
        queryClient.invalidateQueries('/users');
      },
      onError: () => {
        console.log('something went wrong') // Todo
      } 
    })
  }
  

  return { addPost, deletePost, editPost };
}

export default usePosts;
