import { useContext } from 'react';
import { SERVER_ADDRESS } from '../common/constants'
import { AuthContext } from '../providers/AuthContext'

const useProfile = () => {
  const { user } = useContext(AuthContext);

  return {
    avatar: user.avatar ? `${SERVER_ADDRESS}/${user.avatar}` : `${SERVER_ADDRESS}/default.png`,
    username: user.username,
    isLoggedIn: user.isLoggedIn,
    isAdmin: user.role === 2,
    token: user.token,
    id: user.id,
    friends: user.friends,
  }
};

export default useProfile;
