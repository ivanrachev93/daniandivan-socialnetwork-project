export const MIN_USERNAME_LENGTH = 4;
export const MAX_USERNAME_LENGTH = 40;
export const MIN_PASSWORD_LENGTH = 6;
export const MAX_PASSWORD_LENGTH = 40;
export const SERVER_ADDRESS = 'http://localhost:5000';

export enum Credentials {
  USERNAME = 'username',
  NEW_PASSWORD = 'newPassword',
  PASSWORD = 'password',
  EMAIL = 'email',
};

export enum ButtonVariants {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  OUTLINE = 'outline',
};

export enum FriendRequestStatus {
  PENDING = 1,
  APPROVED,
};

export enum FriendshipStatus {
  FRIENDS = 1,
  I_CANNOT_ACCEPT,
  I_CAN_ACCEPT,
  NOT_FRIENDS,
};

export enum Reactions {
  LIKE = 1,
  LOVE,
};
