import decode from 'jwt-decode';
import React from 'react';
import { createEmitAndSemanticDiagnosticsBuilderProgram } from 'typescript';
import { getUserById } from '../services/requests';
import { UserContextType } from '../types/AuthContextTypes';
import { SimpleUserType, UserType } from '../types/UserTypes';
import { FriendRequestStatus, FriendshipStatus, Reactions } from './constants';

export const updateUserForm = <T>(
  prop: string,
  value: string,
  setState: React.Dispatch<React.SetStateAction<T>>
) => {
  setState((prev) => {
    return {
      ...prev,
      [prop]: {
        touched: true,
        value,
      }
    };
  });
};

interface DecodedTokenType {
  email: string;
  exp: number;
  iat: number;
  id: number;
  role: number;
  username: string;
}

export const decodeToken = (token: string): DecodedTokenType => decode(token);

const getToken = () => localStorage.getItem('token') || '';

export const extractUserInfo = async (
  userId: number,
  token: string,
) => {
  const userInfo: UserType = await getUserById(userId, token).then(data => {
    return data;
  });
  return userInfo;
};

export const evaluateFriendship = (
  frStatus: number,
  userCanAccept: boolean,
) => {
  if (frStatus === FriendRequestStatus.APPROVED) {
    return FriendshipStatus.FRIENDS;
  }

  return userCanAccept
    ? FriendshipStatus.I_CANNOT_ACCEPT
    : FriendshipStatus.I_CAN_ACCEPT;
};

export const updateReactionsCount = (
  reactionState: Reactions | null,
  lastReaction: Reactions,
  setCounter: React.Dispatch<React.SetStateAction<{likes: number, loves: number}>>
) => {
  if (!reactionState) {
    lastReaction === Reactions.LIKE
      ? setCounter(prev => ({...prev, likes: prev.likes + 1}))
      : setCounter(prev => ({...prev, loves: prev.loves + 1}));
  } else if (reactionState === lastReaction) {
    lastReaction === Reactions.LIKE
      ? setCounter(prev => ({...prev, likes: prev.likes - 1}))
      : setCounter(prev => ({...prev, loves: prev.loves - 1}));
  } else {
    lastReaction === Reactions.LIKE
      ? setCounter(prev => ({loves: prev.loves - 1, likes: prev.likes + 1}))
      : setCounter(prev => ({likes: prev.likes - 1, loves: prev.loves + 1}));
  }
};

const initialUserLogged: UserContextType = {
  username: '',
  id: 0,
  avatar: null,
  token: '',
  role: 0,
  email: '',
  isLoggedIn: false,
  friends: []
};

export const getUserFromToken = () => {
  try {
    const token = getToken();
    const userInfo = decodeToken(token);

    return {
      ...initialUserLogged,
      isLoggedIn: true,
      token,
      ...userInfo,
    };
  } catch {
    return initialUserLogged;
  }
};

const isValidURL = (link: string) => {
  let url;

  try {
    url = new URL(link);
  } catch (error) {
    return false;  
  }

  return !!url;
}

export const extractVideoId = (url: string) => {
  if (!isValidURL(url)) return 'not a valid url';
  
  const parsed = url.split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
  return (parsed[2] !== undefined) ? parsed[2].split(/[^0-9a-z_-]/i)[0] : parsed[0];
};

export const extractFriendRequests = (friends: SimpleUserType[]) => {
  if (!friends) return [];

  return friends.filter(user => (
    user.friendshipStatus === FriendRequestStatus.PENDING &&
    user.canAcceptFriendship
  ));
};
