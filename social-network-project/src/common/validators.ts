import {
  MAX_PASSWORD_LENGTH,
  MAX_USERNAME_LENGTH,
  MIN_PASSWORD_LENGTH,
  MIN_USERNAME_LENGTH
} from './constants';

const emailRegEx = /^([a-z\d-\.]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/;

export const validateEmail = (email: string) => {
  return (emailRegEx.test(email));
};

export const validateUsername = (username: string) => {
  return username.length >= MIN_USERNAME_LENGTH && username.length <= MAX_USERNAME_LENGTH;
};

export const validatePassword = (password: string) => {
  return password.length >= MIN_PASSWORD_LENGTH && password.length <= MAX_PASSWORD_LENGTH;
};