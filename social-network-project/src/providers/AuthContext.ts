import { createContext } from 'react';
import { AuthContextType } from '../types/AuthContextTypes';

const authContextObj: AuthContextType = {
  user: {
    username: '',
    id: 0,
    avatar: '',
    token: '',
    role: 0,
    email: '',
    isLoggedIn: false,
    friends: []
  },
  setAuth: () => {},
  trigger: false,
  setTrigger: () => {},
};

export const AuthContext = createContext(authContextObj);