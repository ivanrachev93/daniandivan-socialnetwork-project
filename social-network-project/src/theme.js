import { extendTheme, theme as base } from '@chakra-ui/react';
import { mode } from '@chakra-ui/theme-tools'

const brandColors = {
  blue: {
    50: '#ececff',
    100: '#c7c8ee',
    200: '#a4a3dd',
    300: '#7f7ece',
    400: '#5a59be',
    500: '#4240a5',
    600: '#333181',
    700: '#24235d',
    800: '#15153a',
    900: '#060619',
  },
  gray: {
    50: '#f1f1f9',
    75: '#ededf2',
    100: '#D8D8EA',
    200: '#b8b8c4',
    300: '#9c9cab',
    400: '#818093',
    500: '#68677b',
    600: '#505060',
    700: '#393945',
    800: '#22222a',
    900: '#131316',
  },
  red: {
    50: '#ffe7ee',
    100: '#f3c2ca',
    200: '#e59ba9',
    300: '#d87386',
    400: '#cc4d64',
    500: '#b2334a',
    600: '#8c273a',
    700: '#651a28',
    800: '#3e0e18',
    900: '#1c0106',
  },
};

const inputFieldStyling = (props) => {
  return {
    background: mode('gray.100', 'gray.900')(props),
    fontSize: ['md', 'xl'],
    _focus: {
      borderColor: mode('gray.600', 'gray.400')(props),
      background: mode('gray.100', 'gray.800')(props),
    },
    _hover: {
      background: mode('gray.100', 'gray.900')(props),
    },
  }
}


const theme = extendTheme({
  config: {
    initialColorMode: 'dark',
    useSystemColorMode: false,
  },
  fonts: {
    heading: `Nunito Sans, ${base.fonts?.heading}`,
    body: `Sora , ${base.fonts?.body}`,
  },
  colors: {
    ...brandColors,
  },
  styles: {
    global: (props) => ({
      body: {
        bg: mode('#dadae0', 'gray.900')(props),
      },
      'input[type="file"]': {
        borderColor: mode('gray.300','gray.700')(props),
        borderStyle: 'dashed',
        _hover: {
          background: mode('gray.50', 'gray.800')(props),
        }
      },
      'input[type="file"]::file-selector-button': {
        display: 'none',
      },
    }),
  },
  sizes: {
    container: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      '2xl': '1440px',
      '3xl': '1600px',
    },
  },
  components: {
    Text: {
      baseStyle: {
        fontSize: ['sm', 'md', 'lg'],
      },
    },
    Input: {
      variants: {
        filled: (props) => ({
          field: inputFieldStyling(props),
        }),
      },
      defaultProps: {
        variant: 'filled',
        size: 'lg',
      },
    },
    Textarea: {
      variants: {
        filled: (props) => inputFieldStyling(props)
      },
      defaultProps: {
        variant: 'filled',
      }
    },
    Button: {
      baseStyle: {
        _focus: {
          boxShadow: 'none',
        },
        _focusVisible: {
          boxShadow: '0 0 0 3px rgba(0, 120, 200, 0.5)',
        },
        fontFamily: 'heading',
        fontWeight: 'extrabold',
        borderRadius: '.2em',
      },
      sizes: {
        sm: {
          fontSize: 'md',
          px: 6,
          py: 2,
        },
        md: {
          fontSize: 'lg',
          px: 7,
          py: 3,
        },
        lg: {
          fontSize: '2xl',
          px: 8,
          py: 3,
        },
        '3xl': {
          fontSize: '4xl',
          px: 8,
          py: 3,
        },
      },
      variants: {
        primary: (props) => ({
          backgroundColor: mode('blue.500', 'red.500')(props),
          color: 'gray.50',
          _hover: {
            backgroundColor: mode('blue.400', 'red.400')(props),
          },
          _active: {
            backgroundColor: mode('blue.300', 'red.300')(props),
          },
        }),
        secondary: {
          backgroundColor: 'gray.500',
          color: 'gray.50',
          _hover: {
            backgroundColor: 'gray.600',
          },
          _active: {
            backgroundColor: 'gray.700',
          },
        },
        outline: (props) => ({
          borderWidth: 2,
          borderColor: mode('gray.900', 'gray.50')(props),
          _hover: {
            background: mode('gray.900', 'gray.50')(props),
            color: mode('gray.50', 'gray.900')(props),
            borderColor: mode('gray.900', 'gray.50')(props),
          },
          _active: {
            background: mode('gray.800', 'gray.100')(props),
            borderColor: mode('gray.800', 'gray.100')(props),
          },
        }),
      },
      defaultProps: {
        variant: 'primary',
      },
    },
  },
  Heading: {
    variants: {
      primary: (props) => ({
        color: mode('gray.200', 'gray.600')(props),
      })
    },
    defaultProps: {
      variant: 'primary',
    }
  },
  Switch: {
    baseStyle: {
      parts: {
        track: {
          display: 'none',
        },
      },
    },
  },
});

export default theme;
