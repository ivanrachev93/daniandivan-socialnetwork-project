import {
  PostDetailedType,
  PostSimpleType,
  PostResponseType,
  ReactionType,
  CreateCommentType
} from '../types/PostTypes';

const API: string = 'http://localhost:5000';

const validatedResponse = (res: Response) => (message: string) => {
  if (res.ok) return res.json();
  throw new Error(message);
}

export interface MessageResponse {
  message: string;
}

interface User {
  username: string,
  password: string,
  email?: string,
};

export const sendRegisterRequest = async (formData: FormData) => {
  return fetch(`${API}/users`, {
    method: 'post',
    body: formData 
  })
    .then(res => res.json())
    .catch(console.log);
};

export const sendRegisterRequestJSON = async (user: User) => {
  return fetch(`${API}/users`, {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'post',
    body:  JSON.stringify(user),
  })
    .then(res => res.json())
    .catch(console.log);
};

export const sendProfileUpdateRequest = async (formData: FormData, token: string) => {
  return fetch(`${API}/users`, {
    headers: {
      'Authorization': `bearer ${token}`
    },
    method: 'put',
    body:  formData,
  })
    .then(res => {
      console.log(res);
      return res.json()
    })
    .catch(console.log);
};

export const sendLoginRequestJSON = async (user: User) => {
  return fetch(`${API}/auth/login`, {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'post',
    body: JSON.stringify(user),
  })
    .then(res => res.json())
    .catch(console.log);
};

export const sendLogoutRequest = async (token: string) => {
  return fetch(`${API}/auth/logout`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'post'
  })
    .then(res => res.json())
    .catch(console.log);
};

export const getUsers = async (
  username: string = '',
  email: string = '',
  page: string | number = '',
  count: string | number = '',
) => {
  return fetch(`${API}/users?name=${username}&email=${email}&count=${count}&page=${page}`, {
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(res => validatedResponse(res)('Something went wrong, couldn\'t get users.'));
};

export const getUserById = async (userId: number, token: string) => {
  return fetch(`${API}/users/${userId}`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'get'
  })
    .then(res => res.json());
};

export const deleteUser = async (userId: number, token: string) => {
  return fetch(`${API}/users/${userId}`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'delete'
  })
    .then(res => validatedResponse(res)('Something went wrong, couldn\'t fetch the delete request'))
    .catch(console.log);
};

export const banUser = async (
  userId: number,
  token: string,
  payload: {
    reason: string,
    period: number,
  }
) => {
  return fetch(`${API}/users/${userId}/ban`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'post',
    body: JSON.stringify(payload)
  })
    .then(res => validatedResponse(res)('Something went wrong, couldn\'t fetch the ban request'))
    .catch(console.log);
};

export const getPublicFeed = async (): Promise<PostSimpleType[]> => {
  return fetch(`${API}/feed/popular`)
    .then(res => validatedResponse(res)('Something went wrong, couldn\'t fetch feed.'));
};

export const getPrivateFeed = async (
    token: string, page: number, count: number, distance?: string
  ): Promise<PostDetailedType[]> => {

  const pageParam = `?page=${page}`;
  const countParam = `&count=${count}`; 
  const distanceParam = distance ? `&distance=${distance}` : ''; 

  return fetch(`${API}/feed/${pageParam}${countParam}${distanceParam}`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'get'
  })
    .then(res => validatedResponse(res)('Something went wrong, couldn\'t fetch feed.'))
};

export const addPostRequest = async (token: string, body: FormData): Promise<PostResponseType> => {
  return fetch(`${API}/posts`, {
    headers: {
      'Authorization': `bearer ${token}`
    },
    method: 'post',
    body: body,
  })
    .then(res => {
      console.log(res);
      return res.json();
    }/* validatedResponse(res)('Something went wrong with adding your post.')*/)
};

export const editPostRequest = async (
    token: string, postId: number, body: FormData
  ): Promise<PostResponseType> => {
  return fetch(`${API}/posts/${postId}`, {
    headers: {
      'Authorization': `bearer ${token}`
    },
    method: 'put',
    body: body,
  })
    .then(res => validatedResponse(res)('Something went wrong with editing your post.'))
};

export const deletePostRequest = async (token: string, id: number): Promise<MessageResponse> => {
  return fetch(`${API}/posts/${id}`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'delete',
  })
    .then(res => validatedResponse(res)('Something went wrong - couldn\'t delete your post.'))
};

export const getPostsByUser = async (userId: number, token: string) => {
  return fetch(`${API}/users/${userId}/posts`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'get'
  })
    .then(res => res.json())
    .catch(console.log);
};

export const sendFriendReq = async (id: number, toId: number, token: string) => {
  return fetch(`${API}/users/${id}/friends/${toId}`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'post' 
  })
    .then(res => res.json())
    .catch(console.log)
};

export const acceptFriendReq = async (id: number, toId: number, token: string) => {
  return fetch(`${API}/users/${id}/friends/${toId}`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'put' 
  })
    .then(res => res.json())
    .catch(console.log)
};

export const unfriendReq = async (id: number, toId: number, token: string) => {
  return fetch(`${API}/users/${id}/friends/${toId}`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'delete'
  })
    .then(res => res.json())
    .catch(console.log);
};

export const sendPostReaction = async (postId: number, reaction: ReactionType, token: string) => {
  return fetch(`${API}/posts/${postId}/react`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'put',
    body: JSON.stringify(reaction)
  })
    .then(res => res.json())
    .catch(console.log);
};

export const addCommentRequest = async (postId: number, comment: CreateCommentType, token: string) => {
  return fetch(`${API}/posts/${postId}/comments`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'post',
    body: JSON.stringify(comment),
  })
    .then(res => validatedResponse(res)('Something went wrong - couldn\'t add your comment.'))
}

export const editCommentRequest = async (commentId: number, comment: CreateCommentType, token: string) => {
  return fetch(`${API}/comments/${commentId}`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'put',
    body: JSON.stringify(comment),
  })
    .then(res => validatedResponse(res)('Something went wrong - couldn\'t edit your comment.'))
}

export const deleteCommentRequest = async (commentId: number, token: string) => {
  return fetch(`${API}/comments/${commentId}`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'delete',
  })
    .then(res => validatedResponse(res)('Something went wrong - couldn\'t delete your comment.'))
}

export const sendCommentReaction = async (commentId: number, reaction: ReactionType, token: string) => {
  return fetch(`${API}/comments/${commentId}/react`, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${token}`
    },
    method: 'put',
    body: JSON.stringify(reaction)
  })
    .then(res => res.json())
    .catch(console.log);
};
