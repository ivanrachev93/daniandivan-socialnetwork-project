import React, { useContext, useEffect, useState } from 'react';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import PageNotFound from './views/PageNotFound/PageNotFound';
import Home from './views/Home/Home';
import { QueryClient, QueryClientProvider } from 'react-query';
import theme from './theme';
import { AuthContext } from './providers/AuthContext';
import LoginRegister from './views/LoginRegister/LoginRegister';
import { extractUserInfo, getUserFromToken } from './common/helpers';
import Users from './views/Users/Users';
import SingleUser from './views/Users/SingleUser';

import { ChakraProvider, Flex, Box } from '@chakra-ui/react';
import { CSSReset } from '@chakra-ui/css-reset';
import About from './views/About/About';

interface RequireAuthProps {
  component: JSX.Element;
}

const Private: React.FC<RequireAuthProps> = ({ component: Component }) => {
  const { user } = useContext(AuthContext);
  const isLoggedIn = user.isLoggedIn;
  
  return isLoggedIn 
    ? Component
    : <Navigate to="/login" />;
};

const Public: React.FC<RequireAuthProps> = ({ component: Component }) => {
  const { user } = useContext(AuthContext);
  const isLoggedIn = user.isLoggedIn;
  
  return !isLoggedIn
    ? Component
    : <Navigate to="/home" />;
};

const reactQueryClient = new QueryClient();

const App = () => {
  const [userLogged, setUserLogged] = useState(() => getUserFromToken());
  const [trigger, setTrigger] = useState(true);

  useEffect(() => {
    if (userLogged.isLoggedIn) {
      extractUserInfo(userLogged.id, userLogged.token)
        .then(userInfo => {
          setUserLogged(prev => ({
            ...prev,
            avatar: userInfo.avatar,
            friends: userInfo.friends,
          }));
      })
    }
  }, [userLogged.id, userLogged.token, userLogged.isLoggedIn, trigger]);

  return (
    <QueryClientProvider client={reactQueryClient}>
      <ChakraProvider theme={theme}>
        <AuthContext.Provider value={{ user: userLogged, setAuth: setUserLogged, trigger, setTrigger }}>
          <CSSReset />
          <BrowserRouter>
            <Box>
              <Header />
              <Flex w="full" minHeight="85vh" justifyContent="center">
                <Routes>
                  <Route path="/*" element={<Navigate replace to="/404" />} />
                  <Route path="/" element={<Navigate replace to="/home" />} />
                  <Route path="/404/*" element={<PageNotFound />} />
                  <Route path="/home/*" element={<Home />} />
                  <Route path="/about/*" element={<About />} />
                  <Route path="/users/*" element={<Users />} />
                  {/* <Route path="/users/*" element={
                    <Private component={<Users />} />
                  } /> */}
                  <Route path="/users/:id/*" element={
                    <Private component={<SingleUser />} />
                  } />
                  <Route path="/login/*" element={
                    <Public component={<LoginRegister />} />
                  } />
                  <Route path="/register/*" element={
                    <Public component={<LoginRegister />} />
                  } />
                </Routes>
              </Flex>
              <Footer />
            </Box>
          </BrowserRouter>
        </AuthContext.Provider>
      </ChakraProvider>
    </QueryClientProvider>
  );
};

export default App;
